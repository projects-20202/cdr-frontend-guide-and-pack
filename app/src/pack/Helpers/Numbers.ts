interface IInteger {
  min: number;
  max: number;
  multipleOf: number;
  validate: (n: number) => boolean;
  randomize: () => number;
  enum: () => number[];
}

export class Integer implements IInteger {
  min: number = Number.MIN_SAFE_INTEGER;
  max: number = Number.MAX_SAFE_INTEGER;
  multipleOf: number = 1;

  constructor(min = Number.MIN_SAFE_INTEGER, max = Number.MAX_SAFE_INTEGER, multipleOf = 1) {
    this.min = Math.round(min);
    this.max = Math.round(max);
    this.multipleOf = Math.round(multipleOf);
  }

  validate = (n: number) => {
    if (Math.round(n) !== n) return false;
    if (n < this.min) return false;
    if (n > this.max) return false;
    // noinspection RedundantIfStatementJS
    if (n % this.multipleOf !== 0) return false;
    return true;
  };

  randomize = () => {
    const value = Math.round(Math.random() * (this.max - this.min)) + this.min;
    return value - (value % this.multipleOf);
  };

  enum = () => {
    const values = [];
    const start =
      this.min % this.multipleOf === 0 ? this.min : this.min - (this.min % this.multipleOf) + this.multipleOf;
    for (let current = start; current <= this.max; current += this.multipleOf) {
      values.push(current);
    }
    return values;
  };
}

interface IFloat {
  min: number;
  max: number;
  validate: (n: number) => boolean;
  randomize: () => number;
}

export class Float implements IFloat {
  min: number = Number.MIN_SAFE_INTEGER;
  max: number = Number.MAX_SAFE_INTEGER;

  constructor(min = Number.MIN_SAFE_INTEGER, max = Number.MAX_SAFE_INTEGER) {
    this.min = Math.round(min * 100) / 100;
    this.max = Math.round(max * 100) / 100;
  }

  validate = (n: number) => {
    if (n < this.min) return false;
    // noinspection RedundantIfStatementJS
    if (n > this.max) return false;
    return true;
  };

  randomize = () => {
    return Math.round(Math.random() * (this.max - this.min) * 100) / 100 + this.min;
  };
}

interface IEnum<T> {
  size: number;
  values: T[];
  validate: (x: T) => boolean;
  randomize: () => T;
}

export class Enum<T> implements IEnum<T> {
  size: number = 0;
  values: T[] = [];
  index: Integer;

  constructor(values: T[]) {
    this.size = values.length;
    this.values = values;
    this.index = new Integer(0, this.size - 1);
  }

  validate = (x: T) => {
    return this.values.indexOf(x) > -1;
  };

  randomize = () => {
    return this.values[this.index.randomize()];
  };
}
