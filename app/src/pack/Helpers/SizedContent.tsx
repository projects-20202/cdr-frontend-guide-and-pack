import * as React from 'react';
import { ReactNode } from 'react';
import { Atom } from './AtomicHierarchy';
import { FreeSize } from '../Layouts/Sizes';

type ChildrenProps = {
  children?: ReactNode;
};

type ClassNameProps = {
  className?: string;
};

const SizedContent: (props: FreeSize & ChildrenProps & ClassNameProps) => JSX.Element = (props) => {
  if (props.w === 0 && props.h === 0) {
    return <></>;
  }
  return (
    <Atom>
      <div
        style={{
          width: props.w !== undefined ? props.w + 'px' : '100%',
          height: props.h !== undefined ? props.h + 'px' : '100%',
        }}
        className={'suppress-overflow ' + props.className}
      >
        {props.children}
      </div>
    </Atom>
  );
};

export default SizedContent;
