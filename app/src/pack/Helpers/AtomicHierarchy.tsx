import * as React from 'react';
import { ChildrenProps } from './PropTypes/ChildrenProps';

export const Atom: (props: ChildrenProps) => JSX.Element = (props) => <React.Fragment>{props.children}</React.Fragment>;

export const Molecule: (props: ChildrenProps) => JSX.Element = (props) => (
  <React.Fragment>{props.children}</React.Fragment>
);

export const Organism: (props: ChildrenProps) => JSX.Element = (props) => (
  <React.Fragment>{props.children}</React.Fragment>
);

export const System: (props: ChildrenProps) => JSX.Element = (props) => (
  <React.Fragment>{props.children}</React.Fragment>
);

export const Template: (props: ChildrenProps) => JSX.Element = (props) => (
  <React.Fragment>{props.children}</React.Fragment>
);

export const Page: (props: ChildrenProps) => JSX.Element = (props) => <React.Fragment>{props.children}</React.Fragment>;
