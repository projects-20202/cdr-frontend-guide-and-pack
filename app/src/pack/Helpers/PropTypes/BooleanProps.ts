export type BooleanProps = {
  toggle?: () => void;
  active: boolean;
};

export type BooleanDisabledProps = {
  disabled: boolean;
};

export const toBoolean: (t: any) => boolean = (t) => !!t;
