export type OnCloseProps = {
  onClose?: () => void;
};

export type OnOpenProps = {
  onOpen?: () => void;
};

export type OnToggleProps = {
  onToggle?: () => void;
};

export type OnClickProps = {
  onClick?: ((e: any) => void) | (() => void);
};

export type OnClickIconProps = {
  onClickIcon?: ((e: any) => void) | (() => void);
};

export type GoToProps = {
  to: string;
};
