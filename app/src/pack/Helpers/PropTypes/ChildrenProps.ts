import { ReactNode } from 'react';

export type ChildrenProps = {
  children?: ReactNode;
};

export type ChildrenItemsProps = {
  items: ReactNode[];
};
