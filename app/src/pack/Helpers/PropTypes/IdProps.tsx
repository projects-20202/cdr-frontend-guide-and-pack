export type IdProps = {
  id?: string;
};

export type IndexProps = {
  index: string | number;
};
