import { LoremIpsum } from 'lorem-ipsum';

const capitalize = (str: string): string => {
  const trimmed = str.trim();
  return trimmed.charAt(0).toUpperCase() + trimmed.slice(1);
};

class MyLorem extends LoremIpsum {
  // noinspection JSUnusedGlobalSymbols (it is used when we use the `lorem` instance of MyLorem class defined below. PStorm do not understand that
  public generateNames(num?: number) {
    return this.generateWords(num)
      .split(' ')
      .map((w) => capitalize(w))
      .join(' ');
  }
}

/**
 * @usage
 * lorem.generateWords(1);
 * lorem.generateSentences(5);
 * lorem.generateParagraphs(7);
 *
 * @type {LoremIpsum}
 */
const lorem = new MyLorem({
  sentencesPerParagraph: {
    max: 4,
    min: 2,
  },
  wordsPerSentence: {
    max: 16,
    min: 4,
  },
});

export default lorem;
