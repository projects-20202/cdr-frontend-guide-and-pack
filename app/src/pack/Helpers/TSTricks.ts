export class TSTricks {
  /**
   * Little tricky method that allow us to convert enum type to union type and values list :
   * INPUT : enum E {A, B, C} ==> The Enum type
   * OUTPUT : const tValues = ['A','B','C'];  ==> The array of all values
   *
   * You must add manually the input above in order to obtain the union type :
   * INPUT : type T = keyof typeof E;
   * OUTPUT : type T = ('A' | 'B' | 'C'); ==> The union type
   * @param myEnum
   */
  static enumToValues(myEnum: object): string[] {
    return Object.keys(myEnum).filter((k) => k !== '0' && !Number.parseInt(k, 10));
  }

  /**
   * Little tricky method that allow us to convert union type to mapped type :
   * INPUT :  type T = ('A' | 'B' | 'C'); const tValues = ['A','B','C'];   ==> The Enum type
   * OUTPUT : {A: true, B: true, C: true};                                 ==> The Mapped type
   *
   * @param values
   * @param f
   */
  static getDefaultValuesForMappedType<T>(values: T[], f?: (t: T) => any): { [K in T as symbol]?: boolean } {
    return values.reduce((p, c) => {
      return { [(c as unknown) as string]: f ? f(c) : true, ...p };
    }, {});
  }
}

export type Map<T> = { [K in string]?: T };
export type BooleanMap = Map<boolean>;
export type StringMap = Map<string>;
