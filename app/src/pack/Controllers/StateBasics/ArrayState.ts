import { Dispatch, SetStateAction, useState } from 'react';

function pushTransformer<S>(v: S): (prev: S[]) => S[] {
  return (prev) => {
    const n = [...prev];
    n.push(v);
    return n;
  };
}

function popTransformer<S>(): (prev: S[]) => S[] {
  return (prev) => {
    const n = [...prev];
    n.pop();
    return n;
  };
}

function removeTransformer<S>(v: S): (prev: S[]) => S[] {
  return (prev) => {
    const n = [...prev];
    return n.filter((v1) => v1 !== v);
  };
}

function uniqueTransformer<S>(): (prev: S[]) => S[] {
  return (prev) => {
    const n = [...prev];
    return n.filter((v1, i, self) => self.indexOf(v1) === i);
  };
}

export type ArrayState<S> = {
  data: S[];
  add: (v: S) => void;
  remove: (v: S) => void;
  pop: () => void;
  unique: () => void;
  update: Dispatch<SetStateAction<S[]>>;
};

export function useArrayState<S>(initialState?: S[] | (() => S[])): ArrayState<S> {
  const [data, updateData] = useState(initialState || []);
  return {
    data,
    add: (v: S) => updateData(pushTransformer<S>(v)),
    remove: (v: S) => updateData(removeTransformer<S>(v)),
    pop: () => updateData(popTransformer<S>()),
    unique: () => updateData(uniqueTransformer<S>()),
    update: updateData,
  };
}
