import { useEffect, useState } from 'react';

// @TODO - Make examples in the guide

const useTextLoader: (fileUrl: Request | string) => string = (fileUrl) => {
  const [mdContent, setMdContent] = useState<string>('# Loading File ...');

  useEffect(() => {
    fetch(fileUrl)
      .then((response) => response.text())
      .then((text) => {
        setMdContent(text);
      });
  });

  return mdContent;
};

export default useTextLoader;
