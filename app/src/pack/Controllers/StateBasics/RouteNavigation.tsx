import { BaseSyntheticEvent } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { toBoolean } from '../../Helpers/PropTypes/BooleanProps';

const useRouteGoBack: () => (e: BaseSyntheticEvent) => void = () => {
  const history = useHistory();
  return (e: BaseSyntheticEvent) => {
    e.stopPropagation();
    history.goBack();
  };
};

const useRouteGoTo: () => (to: string) => (e: BaseSyntheticEvent) => void = () => {
  const history = useHistory();
  return (to: string) => (e: BaseSyntheticEvent) => {
    e.stopPropagation();
    history.push(to);
  };
};

const useRouteMatchExact: (to: string) => boolean = (to) => {
  const match = useRouteMatch(to);

  return toBoolean(match && match.isExact);
};

const useRouteMatchParams: (to: string) => any = (to) => {
  const match = useRouteMatch(to);

  return match && match.params;
};

export { useRouteGoBack, useRouteGoTo, useRouteMatchExact, useRouteMatchParams };
