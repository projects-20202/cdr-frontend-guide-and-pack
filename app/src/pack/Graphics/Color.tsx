import * as React from 'react';

export type ColorHue = 'alpha' | 'beta' | 'gamma' | 'delta' | 'omega';
export type ColorSat = 'fader' | 'fade' | 'pastel' | 'bright' | 'brighter';
export type ColorValue = 'darker' | 'dark' | 'dusky' | 'light';
