import { useMediaQuery } from 'react-responsive';

export type Size = {
  w: number;
  h: number;
};

export type FreeSize = {
  w?: number;
  h?: number;
};

export type FixedWidth = { width: number };
export type PictureAndLegendSize = {
  pictureSize: Size;
  legendSize: Size;
};

type Sizes = {
  MINI: Size;
  TINY: Size;
  SMALL: Size;
  NORMAL: Size;
  PLUS: Size;
  LARGE: Size;
  HUGE: Size;
};

export const Square: (w: number) => Size = (w) => {
  return { w, h: w };
};

export const PortraitShape: (w: number) => Size = (w) => {
  return { w, h: (4 * w) / 3 };
};

export const LandscapeShape: (w: number) => Size = (w) => {
  const portraitH = (4 * w) / 3;
  return { w: (4 * portraitH) / 3, h: portraitH };
};

export const Band: (w: number) => Size = (w) => {
  return { w, h: w / 3 };
};

export const ThinBand: (w: number) => Size = (w) => {
  return { w, h: w / 6 };
};

export const displaySize: (s: FreeSize) => string = (s) => {
  return '[w: ' + s.w?.toFixed(1) + '; h:' + s.h?.toFixed(1) + ']';
};

export const SizeZero = {
  w: 0,
  h: 0,
};

export const GapBases: Sizes = {
  MINI: PortraitShape(3),
  TINY: PortraitShape(6),
  SMALL: PortraitShape(9),
  NORMAL: PortraitShape(12),
  PLUS: PortraitShape(18),
  LARGE: PortraitShape(24),
  HUGE: PortraitShape(30),
};

export const SquareBases: Sizes = {
  MINI: Square(37.5),
  TINY: Square(75),
  SMALL: Square(150),
  NORMAL: Square(225),
  PLUS: Square(300),
  LARGE: Square(600),
  HUGE: Square(1200),
};

export const PicsBases: Sizes = {
  MINI: PortraitShape(37.5),
  TINY: PortraitShape(75),
  SMALL: PortraitShape(150),
  NORMAL: PortraitShape(225),
  PLUS: PortraitShape(300),
  LARGE: PortraitShape(600),
  HUGE: PortraitShape(1200),
};

export const LandscapePicsBases: Sizes = {
  MINI: LandscapeShape(37.5),
  TINY: LandscapeShape(75),
  SMALL: LandscapeShape(150),
  NORMAL: LandscapeShape(225),
  PLUS: LandscapeShape(300),
  LARGE: LandscapeShape(600),
  HUGE: LandscapeShape(1200),
};

export const BandBases: Sizes = {
  MINI: Band(37.5),
  TINY: Band(75),
  SMALL: Band(150),
  NORMAL: Band(225),
  PLUS: Band(300),
  LARGE: Band(600),
  HUGE: Band(1200),
};

// @TODO - improve that with all width limits

export const useHighScreen: (h?: number) => boolean = (h = 960) =>
  useMediaQuery({ query: '(min-height: ' + h + 'px)' });

export const useMiniScreen: (w?: number) => boolean = () => useMediaQuery({ query: '(max-width: ' + 400 + 'px)' });

export const useTinyScreen: (w?: number) => boolean = () =>
  useMediaQuery({ query: '(min-width: ' + 400 + 'px) and (max-width: ' + 600 + 'px)' });

export const useSmallScreen: (w?: number) => boolean = () =>
  useMediaQuery({ query: '(min-width: ' + 600 + 'px) and (max-width: ' + 900 + 'px)' });

export const useNormalScreen: (w?: number) => boolean = () =>
  useMediaQuery({ query: '(min-width: ' + 900 + 'px) and (max-width: ' + 1200 + 'px)' });

export const useLargeScreen: (w?: number) => boolean = (w = 900) =>
  useMediaQuery({ query: '(min-width: ' + 1200 + 'px) and (max-width: ' + 1800 + 'px)' });

export const useHugeScreen: (w?: number) => boolean = (w = 900) =>
  useMediaQuery({ query: '(min-width: ' + 1800 + 'px)' });

export const useScreenWidth: () => 'mini' | 'tiny' | 'small' | 'normal' | 'large' | 'huge' = () => {
  const mini = useMiniScreen();
  const tiny = useTinyScreen();
  const small = useSmallScreen();
  const normal = useNormalScreen();
  const large = useLargeScreen();
  const huge = useHugeScreen();

  if (mini) return 'mini';
  if (tiny) return 'tiny';
  if (small) return 'small';
  if (normal) return 'normal';
  if (large) return 'large';
  if (huge) return 'huge';
  return 'normal';
};
export const mediasQueries: string[] = [];
