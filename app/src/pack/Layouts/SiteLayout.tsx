import * as React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Template } from '../Helpers/AtomicHierarchy';
import { useHighScreen } from './Sizes';

type SiteLayoutProps = {
  header: () => JSX.Element;
  header2: () => JSX.Element;
  brand: () => JSX.Element;
  main: () => JSX.Element;
  footer: () => JSX.Element;
};

function SiteLayout(props: SiteLayoutProps) {
  return (
    <Template>
      <Router>
        <div id="site">
          <header id="site-header">
            <nav id="header-nav" className="color-alpha-fader-light">
              {props.header()}
            </nav>
            {useHighScreen() && (
              <div id="header-brand" className="color-alpha-bright-light center">
                {props.brand()}
              </div>
            )}
            <nav id="header-nav-2" className="color-alpha-bright-dark">
              {props.header2()}
            </nav>
          </header>
          <div id="site-content">
            <main id="site-main" className="color-alpha-fader-light">
              {props.main()}
            </main>
            <footer id="site-footer" className="color-alpha-bright-light center">
              {props.footer()}
            </footer>
          </div>
        </div>
      </Router>
    </Template>
  );
}

export default SiteLayout;
