import * as React from 'react';
import { Template } from '../../Helpers/AtomicHierarchy';
import { ChildrenProps } from '../../Helpers/PropTypes/ChildrenProps';

const MainForm: (props: ChildrenProps) => JSX.Element = (props) => (
  <Template>
    <form
      className="main-form"
      onSubmit={(event) => {
        event.preventDefault();
        return false;
      }}
    >
      {props.children}
    </form>
  </Template>
);

export default MainForm;
