import * as React from 'react';
import { Template } from '../../Helpers/AtomicHierarchy';
import { ChildrenProps } from '../../Helpers/PropTypes/ChildrenProps';
import { OnCloseProps } from '../../Helpers/PropTypes/ActionProps';
import { ClassNameProps } from '../../index';
import Button from '../../Components/Atoms/Controls/Button';

const ModalWindowGeneric: (props: ChildrenProps & OnCloseProps & ClassNameProps) => JSX.Element = (props) => {
  return (
    <Template>
      <div className={props.className}>
        <div className="close-button" onClick={() => props.onClose?.()}>
          X
        </div>
        <div className="section-container">{props.children}</div>
        <footer className="section-container">
          <div className="modal-end">
            <Button onClick={() => props.onClose?.()} className="color-beta-bright-light center modal-button">
              Validate
            </Button>
          </div>
        </footer>
      </div>
    </Template>
  );
};

const ModalWindowFix: (props: ChildrenProps & OnCloseProps) => JSX.Element = (props) => (
  <ModalWindowGeneric className="modal-window-fix" {...props} />
);

const ModalWindowFluid: (props: ChildrenProps & OnCloseProps) => JSX.Element = (props) => (
  <ModalWindowGeneric className="modal-window-fluid" {...props} />
);

export { ModalWindowFix, ModalWindowFluid };
