import * as React from 'react';
import { Template } from '../../Helpers/AtomicHierarchy';
import { ChildrenProps } from '../../Helpers/PropTypes/ChildrenProps';

const ModalForm: (props: ChildrenProps) => JSX.Element = (props) => (
  <Template>
    <form
      className="modal-form"
      onSubmit={(event) => {
        event.preventDefault();
        return false;
      }}
    >
      {props.children}
    </form>
  </Template>
);

export default ModalForm;
