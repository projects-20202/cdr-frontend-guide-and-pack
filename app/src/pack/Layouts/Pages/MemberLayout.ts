import { BandBases, GapBases, PicsBases, Size, SizeZero, useScreenWidth } from '../../index';
import { FreeSize, SquareBases } from '../Sizes';

type MemberPageLayoutSizes = {
  mainPictureSize: Size;
  secondaryPictureSize: Size;
  secondaryPictureLegendSize: Size;
  bandSize: FreeSize;
  gapSize: Size;
};
const useMemberPageLayoutSizes: () => MemberPageLayoutSizes = () => {
  const screen = useScreenWidth();
  switch (screen) {
    case 'mini':
      return {
        mainPictureSize: PicsBases.TINY,
        secondaryPictureSize: PicsBases.MINI,
        secondaryPictureLegendSize: SizeZero,
        bandSize: {},
        gapSize: GapBases.TINY,
      };
    case 'tiny':
      return {
        mainPictureSize: PicsBases.SMALL,
        secondaryPictureSize: PicsBases.TINY,
        secondaryPictureLegendSize: SizeZero,
        bandSize: {},
        gapSize: GapBases.SMALL,
      };
    case 'small':
      return {
        mainPictureSize: PicsBases.SMALL,
        secondaryPictureSize: PicsBases.TINY,
        secondaryPictureLegendSize: SizeZero,
        bandSize: PicsBases.SMALL,
        gapSize: GapBases.SMALL,
      };
    case 'normal':
      return {
        mainPictureSize: PicsBases.NORMAL,
        secondaryPictureSize: PicsBases.SMALL,
        secondaryPictureLegendSize: BandBases.SMALL,
        bandSize: SquareBases.NORMAL,
        gapSize: GapBases.NORMAL,
      };
    case 'large':
      return {
        mainPictureSize: PicsBases.PLUS,
        secondaryPictureSize: PicsBases.SMALL,
        secondaryPictureLegendSize: BandBases.SMALL,
        bandSize: BandBases.PLUS,
        gapSize: GapBases.PLUS,
      };
    case 'huge':
    default:
      return {
        mainPictureSize: PicsBases.LARGE,
        secondaryPictureSize: PicsBases.NORMAL,
        secondaryPictureLegendSize: BandBases.NORMAL,
        bandSize: BandBases.LARGE,
        gapSize: GapBases.PLUS,
      };
  }
};

export default useMemberPageLayoutSizes;
