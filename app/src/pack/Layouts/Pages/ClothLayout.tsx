import { BandBases, GapBases, PicsBases, Size, useScreenWidth } from '../../index';
import { FreeSize } from '../Sizes';

type ClothPageLayoutSizes = {
  mainPictureSize: Size;
  brandSize: FreeSize;
  calendarSize: Size;
  descriptionSize: FreeSize;
  secondaryPictureSize: Size;
  recommendationPictureSize: Size;
  recommendationPictureLegendSize: Size;
};

const miniLayout: () => ClothPageLayoutSizes = () => {
  const mainPictureSize = {
    w: PicsBases.SMALL.w + GapBases.TINY.w,
    h: PicsBases.SMALL.h + GapBases.TINY.h,
  };
  return {
    mainPictureSize,
    brandSize: { h: PicsBases.SMALL.h / 2 },
    calendarSize: mainPictureSize,
    descriptionSize: { h: PicsBases.SMALL.h / 2 },
    secondaryPictureSize: PicsBases.TINY,
    recommendationPictureSize: PicsBases.TINY,
    recommendationPictureLegendSize: BandBases.TINY,
  };
};

const tinyLayout: () => ClothPageLayoutSizes = () => {
  const h = PicsBases.SMALL.h + GapBases.SMALL.h;
  const w = PicsBases.SMALL.w + GapBases.SMALL.w;
  return {
    mainPictureSize: { w, h },
    brandSize: { h: PicsBases.SMALL.h / 2 },
    calendarSize: { w, h },
    descriptionSize: { h: PicsBases.SMALL.h / 2 },
    secondaryPictureSize: PicsBases.TINY,
    recommendationPictureSize: PicsBases.TINY,
    recommendationPictureLegendSize: BandBases.TINY,
  };
};

const smallLayout: () => ClothPageLayoutSizes = () => {
  const h = PicsBases.NORMAL.h + GapBases.SMALL.h;
  const w = PicsBases.NORMAL.w + GapBases.SMALL.w;
  return {
    mainPictureSize: { w, h },
    brandSize: { h: PicsBases.NORMAL.h / 2 },
    calendarSize: { w, h },
    descriptionSize: { h: PicsBases.NORMAL.h / 2 },
    secondaryPictureSize: PicsBases.TINY,
    recommendationPictureSize: PicsBases.TINY,
    recommendationPictureLegendSize: BandBases.TINY,
  };
};

const normalLayout: () => ClothPageLayoutSizes = () => {
  const h = PicsBases.NORMAL.h + GapBases.NORMAL.h;
  const w = PicsBases.NORMAL.w + GapBases.NORMAL.w;
  return {
    mainPictureSize: { w, h },
    brandSize: { h: PicsBases.NORMAL.h / 2 },
    calendarSize: { w, h },
    descriptionSize: { h: PicsBases.NORMAL.h / 2 },
    secondaryPictureSize: PicsBases.SMALL,
    recommendationPictureSize: PicsBases.SMALL,
    recommendationPictureLegendSize: BandBases.SMALL,
  };
};
const largeLayout: () => ClothPageLayoutSizes = () => {
  const calendarSize = {
    h: PicsBases.NORMAL.h + GapBases.PLUS.h,
    w: PicsBases.NORMAL.w + GapBases.PLUS.w,
  };
  return {
    mainPictureSize: PicsBases.LARGE,
    brandSize: { h: PicsBases.NORMAL.h / 2 },
    calendarSize,
    descriptionSize: { h: PicsBases.NORMAL.h / 2 },
    secondaryPictureSize: PicsBases.NORMAL,
    recommendationPictureSize: PicsBases.SMALL,
    recommendationPictureLegendSize: BandBases.SMALL,
  };
};

const hugeLayout: () => ClothPageLayoutSizes = () => {
  const gap = GapBases.PLUS.h;
  const mainPictureHeight = PicsBases.LARGE.h;
  const calendarHeight = PicsBases.PLUS.h;
  const secondaryLayerHeight = 2 * PicsBases.NORMAL.h + gap;
  const secondRowHeight = calendarHeight;

  const thirdRowHeight = secondaryLayerHeight - calendarHeight - gap;
  const firstRowHeight = mainPictureHeight - secondRowHeight - thirdRowHeight - 2 * gap;

  return {
    mainPictureSize: PicsBases.LARGE,
    brandSize: { h: firstRowHeight },
    calendarSize: PicsBases.PLUS,
    descriptionSize: { h: firstRowHeight },
    secondaryPictureSize: PicsBases.NORMAL,
    recommendationPictureSize: PicsBases.SMALL,
    recommendationPictureLegendSize: BandBases.SMALL,
  };
};

const useClothPageLayoutSizes: () => ClothPageLayoutSizes = () => {
  const screen = useScreenWidth();

  switch (screen) {
    case 'mini':
      return miniLayout();
    case 'tiny':
      return tinyLayout();
    case 'small':
      return smallLayout();
    case 'large':
      return largeLayout();
    case 'huge':
      return hugeLayout();
    case 'normal':
    default:
      return normalLayout();
  }
};

export default useClothPageLayoutSizes;
