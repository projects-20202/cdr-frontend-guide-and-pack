export type BiColorRatioProps = {
  ratio?: 50 | 75 | 90;
};

export type BiColorClassProps = {
  firstColorClass: string;
  secondColorClass: string;
};
