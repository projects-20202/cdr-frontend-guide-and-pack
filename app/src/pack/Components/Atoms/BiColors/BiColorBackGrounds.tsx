import * as React from 'react';
import { Atom } from '../../../Helpers/AtomicHierarchy';
import { BiColorClassProps, BiColorRatioProps } from './BiColorProps';
import SizedContent from '../../../Helpers/SizedContent';
import { Size } from '../../../Layouts/Sizes';

const BiColorBackGround: (props: BiColorClassProps & Size & BiColorRatioProps) => JSX.Element = (props) => {
  const ratio = props.ratio ? props.ratio : 50;
  const shift = (10 * props.h) / props.w;

  return (
    <Atom>
      <SizedContent h={props.h} w={props.w}>
        <svg viewBox="0 0 100 100" version="1.1" preserveAspectRatio="none" className="maximize">
          <polygon
            className={props.firstColorClass}
            strokeWidth={0}
            points={'0,0 ' + (ratio + shift) + ',0 ' + (ratio - shift) + ',100 0,100'}
          />
          <polygon
            className={props.secondColorClass}
            strokeWidth={0}
            points={'' + (ratio + shift) + ',0 100,0 100,100 ' + (ratio - shift) + ',100'}
          />
        </svg>
      </SizedContent>
    </Atom>
  );
};

export default BiColorBackGround;
