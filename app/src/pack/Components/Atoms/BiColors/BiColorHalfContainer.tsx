import * as React from 'react';
import { Atom } from '../../../Helpers/AtomicHierarchy';
import { ChildrenProps } from '../../../Helpers/PropTypes/ChildrenProps';
import { ClassNameProps } from '../../../Helpers/PropTypes/ClassNameProps';
import { BiColorRatioProps } from './BiColorProps';
import SizedContent from '../../../Helpers/SizedContent';
import { Size } from '../../../Layouts/Sizes';

const BiColorHalfContainer: (props: Size & ChildrenProps & ClassNameProps & BiColorRatioProps) => JSX.Element = (
  props,
) => {
  return (
    <Atom>
      <SizedContent h={props.h} w={props.w}>
        <div className={'article-container maximize ' + props.className}>{props.children}</div>
      </SizedContent>
    </Atom>
  );
};

export default BiColorHalfContainer;
