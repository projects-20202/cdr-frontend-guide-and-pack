import * as React from 'react';
import { HSVType } from './HSV';
import { ChildrenProps } from '../../../Helpers/PropTypes/ChildrenProps';
import { OnClickProps } from '../../../Helpers/PropTypes/ActionProps';
import { Atom } from '../../../Helpers/AtomicHierarchy';
import { ClassNameProps } from '../../../index';

interface ColorProps {
  color: HSVType;
}

const HSVColor: (props: ColorProps & ChildrenProps & ClassNameProps & OnClickProps) => JSX.Element = (props) => {
  return (
    <Atom>
      <div
        className={'center ' + props.className}
        onClick={props.onClick}
        style={{
          backgroundColor: props.color ? props.color.toHex() : 'lightgray',
          color: props.color.complementaryColor().toHex(),
        }}
      >
        {props.children}
      </div>
    </Atom>
  );
};
export default HSVColor;
