import * as React from 'react';
import { BooleanProps } from '../../../Helpers/PropTypes/BooleanProps';
import { Atom } from '../../../Helpers/AtomicHierarchy';

const CheckBoxButton: (props: BooleanProps) => JSX.Element = (props) => {
  return (
    <Atom>
      {props.active ? (
        <div className="icon-square-full" onClick={() => props.toggle?.()} />
      ) : (
        <div className="icon-square" onClick={() => props.toggle?.()} />
      )}
    </Atom>
  );
};

export default CheckBoxButton;
