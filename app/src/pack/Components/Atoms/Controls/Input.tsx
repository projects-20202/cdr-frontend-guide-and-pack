import * as React from 'react';
import { IdProps } from '../../../Helpers/PropTypes/IdProps';
import { Atom } from '../../../Helpers/AtomicHierarchy';

type GenericInputProps = {
  type: string;
};
type InputProps<T> = {
  label?: string;
  placeHolder?: string;
  value: T;
  mapChange: (newValue: T) => void;
};
type StringToGeneric<T> = {
  stringToGenericType: (value: string) => T;
};
type GenericToString<T> = {
  genericTypeToHtmlInputValue: (value: T) => string | number;
};

function GenericInput<T>(
  props: GenericInputProps & InputProps<T> & IdProps & StringToGeneric<T> & GenericToString<T>,
): JSX.Element {
  return (
    <Atom>
      {props.label && <label htmlFor={props.id}>{props.label}</label>}
      <input
        id={props.id}
        type={props.type}
        value={props.genericTypeToHtmlInputValue(props.value)}
        placeholder={props.placeHolder || props.label}
        onChange={(event) => props.mapChange(props.stringToGenericType(event.target.value))}
      />
    </Atom>
  );
}

const InputText: (props: InputProps<string> & IdProps) => JSX.Element = (props) => {
  return (
    <GenericInput<string>
      type="text"
      label={props.label}
      value={props.value}
      placeHolder={props.placeHolder}
      mapChange={props.mapChange}
      id={props.id}
      stringToGenericType={(v: string) => v}
      genericTypeToHtmlInputValue={(v: string) => v}
    />
  );
};

const InputPassword: (props: InputProps<string> & IdProps) => JSX.Element = (props) => {
  return (
    <GenericInput<string>
      type="password"
      label={props.label}
      value={props.value}
      placeHolder={props.placeHolder}
      mapChange={props.mapChange}
      id={props.id}
      stringToGenericType={(v: string) => v}
      genericTypeToHtmlInputValue={(v: string) => v}
    />
  );
};

const InputNumber: (props: InputProps<number> & IdProps) => JSX.Element = (props) => {
  return (
    <GenericInput<number>
      type="number"
      label={props.label}
      value={props.value}
      placeHolder={props.placeHolder}
      mapChange={props.mapChange}
      id={props.id}
      stringToGenericType={(v: string) => Number.parseFloat(v)}
      genericTypeToHtmlInputValue={(v: number) => v}
    />
  );
};

export { InputText, InputPassword, InputNumber };
