import * as React from 'react';
import { BooleanProps } from '../../../Helpers/PropTypes/BooleanProps';
import { Atom } from '../../../Helpers/AtomicHierarchy';

const Toggle: (props: BooleanProps) => JSX.Element = (props) => {
  const styleRect: React.SVGProps<SVGRectElement> = {
    fill: 'lightgrey',
    fillOpacity: 1,
    stroke: '#000000',
    strokeWidth: 0.25,
    strokeLinejoin: 'round',
    strokeMiterlimit: 4,
    strokeDasharray: 'none',
    strokeOpacity: 1,
  };
  const styleCircle: React.SVGProps<SVGEllipseElement> = {
    fill: props.active ? '#00bbff' : 'gray',
    fillOpacity: 1,
    stroke: '#000000',
    strokeWidth: 0.25,
    strokeLinejoin: 'round',
    strokeMiterlimit: 4,
    strokeDasharray: 'none',
    strokeOpacity: 1,
  };
  return (
    <Atom>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="75"
        height="50"
        viewBox="0 0 100 100"
        version="1.1"
        onClick={props.toggle}
      >
        <g>
          <rect width={70} height={20} x={15} y={40} {...styleRect} />
          <ellipse cx={props.active ? 75 : 25} cy={50} rx={20} ry={20} {...styleCircle} />
        </g>
      </svg>
    </Atom>
  );
};

export default Toggle;
