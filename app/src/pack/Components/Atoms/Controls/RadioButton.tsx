import * as React from 'react';
import { BooleanProps } from '../../../Helpers/PropTypes/BooleanProps';
import { Atom } from '../../../Helpers/AtomicHierarchy';

const RadioButton: (props: BooleanProps) => JSX.Element = (props) => {
  return (
    <Atom>
      {props.active ? (
        <div className="icon-disk" onClick={() => props.toggle?.()} />
      ) : (
        <div className="icon-circle" onClick={() => props.toggle?.()} />
      )}
    </Atom>
  );
};

export default RadioButton;
