import * as React from 'react';
import { ChildrenProps } from '../../../Helpers/PropTypes/ChildrenProps';
import { IdProps } from '../../../Helpers/PropTypes/IdProps';
import { ClassNameProps } from '../../../Helpers/PropTypes/ClassNameProps';
import { OnClickProps } from '../../../Helpers/PropTypes/ActionProps';
import { Atom } from '../../../Helpers/AtomicHierarchy';

const Button: (props: ChildrenProps & IdProps & ClassNameProps & OnClickProps) => JSX.Element = (props) => {
  return (
    <Atom>
      <button onClick={props.onClick} id={props.id} className={props.className}>
        {props.children}
      </button>
    </Atom>
  );
};

export default Button;
