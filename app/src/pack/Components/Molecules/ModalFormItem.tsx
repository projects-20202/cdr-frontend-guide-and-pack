import * as React from 'react';
import { Molecule } from '../../Helpers/AtomicHierarchy';

export type ModalFormItemProps = {
  content: JSX.Element | string;
  control: JSX.Element | string;
};
const ModalFormItem: (props: ModalFormItemProps) => JSX.Element = (props) => {
  return (
    <Molecule>
      <div>
        {props.content}
        <div className="modal-form-control">{props.control}</div>
      </div>
    </Molecule>
  );
};

export default ModalFormItem;
