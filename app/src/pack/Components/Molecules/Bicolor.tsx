import { Molecule } from '../../Helpers/AtomicHierarchy';
import BiColorHalfContainer from '../Atoms/BiColors/BiColorHalfContainer';
import * as React from 'react';
import BiColorBackGround from '../Atoms/BiColors/BiColorBackGrounds';
import { BiColorClassProps, BiColorRatioProps } from '../Atoms/BiColors/BiColorProps';
import { Size } from '../../Layouts/Sizes';
import SizedContent from '../../Helpers/SizedContent';

type BiColorChildrenProps = {
  firstContent: JSX.Element;
  secondContent: JSX.Element;
};

const BiColor: (props: BiColorClassProps & BiColorRatioProps & Size & BiColorChildrenProps) => JSX.Element = (
  props,
) => {
  const shift = (10 * props.h) / props.w;
  const ratio = props.ratio ? props.ratio : 50;

  const leftWidth = (props.w * (ratio - shift)) / 100;
  const rightWidth = (props.w * (100 - ratio - shift)) / 100;

  return (
    <Molecule>
      <SizedContent h={props.h} w={props.w}>
        <div className="overlay-container maximize maximize-cascade">
          <div className="overlay-layer">
            <BiColorBackGround
              firstColorClass={props.firstColorClass}
              secondColorClass={props.secondColorClass}
              h={props.h}
              w={props.w}
              ratio={ratio}
            />
          </div>
          <div className="overlay-layer" style={{ right: 100 - ratio - shift + '%' }}>
            <BiColorHalfContainer h={props.h} w={leftWidth} className={props.firstColorClass} ratio={ratio}>
              {props.firstContent}
            </BiColorHalfContainer>
          </div>
          <div className="overlay-layer" style={{ left: ratio + shift + '%' }}>
            <BiColorHalfContainer h={props.h} w={rightWidth} className={props.secondColorClass} ratio={ratio}>
              {props.secondContent}
            </BiColorHalfContainer>
          </div>
        </div>
      </SizedContent>
    </Molecule>
  );
};

export default BiColor;
