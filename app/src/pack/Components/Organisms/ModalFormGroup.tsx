import * as React from 'react';
import { Organism } from '../../Helpers/AtomicHierarchy';
import ModalFormItem, { ModalFormItemProps } from '../Molecules/ModalFormItem';

export type ModalFormGroupProps = {
  title: JSX.Element | string;
  titleControl?: JSX.Element | string;
  items: ModalFormItemProps[];
};
const ModalFormGroup: (props: ModalFormGroupProps) => JSX.Element = (props) => {
  return (
    <Organism>
      <div className="modal-form-group">
        <label>
          {props.title}
          {props.titleControl && <div className="modal-form-control">{props.titleControl}</div>}
        </label>
        {props.items.map((item: ModalFormItemProps, index) => {
          return <ModalFormItem key={index} content={item.content} control={item.control} />;
        })}
      </div>
    </Organism>
  );
};

export default ModalFormGroup;
