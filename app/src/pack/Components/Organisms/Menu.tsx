import * as React from 'react';
import { ReactNode } from 'react';

import { Link } from 'react-router-dom';
import { Template } from '../../Helpers/AtomicHierarchy';

export type MenuItemLink = {
  to: string;
  label: string;
  content?: ReactNode;
};

export type MenuItemAction = {
  action: ((e: any) => void) | (() => void);
  label: string;
  content?: ReactNode;
};

export type MenuItemProps = MenuItemLink | MenuItemAction;

function isLink(item: MenuItemProps): item is MenuItemLink {
  return 'to' in item;
}

const MenuItem: (props: { item: MenuItemProps }) => JSX.Element = (props) => {
  return (
    <li className="hover-color-alpha-fade-light maximize center">
      <div className="menu-decoration icon-cloth" />
      {isLink(props.item) ? (
        <Link className="" to={props.item.to} aria-label={props.item.label}>
          {props.item.content || props.item.label}
        </Link>
      ) : (
        <div className="" onClick={props.item.action}>
          {props.item.content || props.item.label}
        </div>
      )}
    </li>
  );
};

type MenuProps = {
  items: MenuItemProps[];
};

const Menu: (props: MenuProps) => JSX.Element = (props) => {
  return (
    <Template>
      <ul className="nav-bar">
        {props.items.map((item, index) => (
          <MenuItem item={item} key={index} />
        ))}
      </ul>
    </Template>
  );
};

export default Menu;
