import Button from '../Components/Atoms/Controls/Button';
import RadioButton from '../Components/Atoms/Controls/RadioButton';
import CheckBoxButton from '../Components/Atoms/Controls/CheckBoxButton';
import Toggle from '../Components/Atoms/Controls/Toggle';
import * as InputsModule from '../Components/Atoms/Controls/Input';
import * as MenuModule from '../Components/Organisms/Menu';
import Menu from '../Components/Organisms/Menu';
import MainForm from '../Layouts/Components/MainForm';
import * as ArrayStateModule from '../Controllers/StateBasics/ArrayState';
import { useArrayState } from '../Controllers/StateBasics/ArrayState';
import useLocalStorage from '../Controllers/StateBasics/LocalStorage';
import useTextLoader from '../Controllers/StateBasics/TextLoader';
import * as RouteNavigationModule from '../Controllers/StateBasics/RouteNavigation';
import * as ColorModule from '../Graphics/Color';
import * as ActionPropsModule from '../Helpers/PropTypes/ActionProps';
import * as BooleanPropsModule from '../Helpers/PropTypes/BooleanProps';
import * as ChildrenPropsModule from '../Helpers/PropTypes/ChildrenProps';
import * as ClassNamePropsModule from '../Helpers/PropTypes/ClassNameProps';
import * as IdPropsModule from '../Helpers/PropTypes/IdProps';
import * as AtomicHierarchyModule from '../Helpers/AtomicHierarchy';
import lorem from '../Helpers/Lorem';
import { Enum, Float, Integer } from '../Helpers/Numbers';
import * as TSTricksModule from '../Helpers/TSTricks';
import { TSTricks } from '../Helpers/TSTricks';

import SizedContent from '../Helpers/SizedContent';
import * as SizesModule from '../Layouts/Sizes';
import SiteLayout from '../Layouts/SiteLayout';
import * as ModalModule from '../Layouts/Components/ModalWindow';
import useMemberPageLayoutSizes from '../Layouts/Pages/MemberLayout';
import HSVColor from '../Components/Atoms/Colors/HSVColor';
import HSV, { HSVType as HSVBaseType } from '../Components/Atoms/Colors/HSV';
// -- Molecules
import BiColor from '../Components/Molecules/Bicolor';
import ModalFormItem, * as ModalItemModule from '../Components/Molecules/ModalFormItem';
// -- Organisms
import ModalFormGroup, * as ModalGroupModule from '../Components/Organisms/ModalFormGroup';

// Components
// -- Atoms
export type HSVType = HSVBaseType;

export const InputText = InputsModule.InputText;
export const InputPassword = InputsModule.InputPassword;
export const InputNumber = InputsModule.InputNumber;

// -- Molecules
export type ModalFormItemProps = ModalItemModule.ModalFormItemProps;

// -- Organisms
export type MenuItemLink = MenuModule.MenuItemLink;
export type MenuItemAction = MenuModule.MenuItemAction;
export type MenuItemProps = MenuModule.MenuItemProps;
export type ModalFormGroupProps = ModalGroupModule.ModalFormGroupProps;

// Graphics
export type ColorHue = ColorModule.ColorHue;
export type ColorSat = ColorModule.ColorSat;
export type ColorValue = ColorModule.ColorValue;

// Controllers
export type ArrayState<S> = ArrayStateModule.ArrayState<S>;
export const useRouteGoBack = RouteNavigationModule.useRouteGoBack;
export const useRouteGoTo = RouteNavigationModule.useRouteGoTo;
export const useRouteMatchExact = RouteNavigationModule.useRouteMatchExact;
export const useRouteMatchParams = RouteNavigationModule.useRouteMatchParams;

// Helpers
// -- PropTypes
export type GoToProps = ActionPropsModule.GoToProps;
export type OnClickIconProps = ActionPropsModule.OnClickIconProps;
export type OnClickProps = ActionPropsModule.OnClickProps;
export type OnCloseProps = ActionPropsModule.OnCloseProps;
export type OnOpenProps = ActionPropsModule.OnOpenProps;

export type OnToggleProps = ActionPropsModule.OnToggleProps;
export type BooleanDisabledProps = BooleanPropsModule.BooleanDisabledProps;
export type BooleanProps = BooleanPropsModule.BooleanProps;
export const toBoolean = BooleanPropsModule.toBoolean;

export type ChildrenItemsProps = ChildrenPropsModule.ChildrenItemsProps;
export type ChildrenProps = ChildrenPropsModule.ChildrenProps;

export type ClassNameProps = ClassNamePropsModule.ClassNameProps;

export type IdProps = IdPropsModule.IdProps;
export type IndexProp = IdPropsModule.IndexProps;

// Helpers
// -- ./

export const Atom = AtomicHierarchyModule.Atom;
export const Molecule = AtomicHierarchyModule.Molecule;
export const Organism = AtomicHierarchyModule.Organism;
export const System = AtomicHierarchyModule.System;
export const Template = AtomicHierarchyModule.Template;
export const Page = AtomicHierarchyModule.Page;

export type Map<T> = TSTricksModule.Map<T>;
export type BooleanMap = TSTricksModule.BooleanMap;
export type StringMap = TSTricksModule.StringMap;

// Layouts
export type Size = SizesModule.Size;
export type FixedWidth = SizesModule.FixedWidth;
export type PictureAndLegendSize = SizesModule.PictureAndLegendSize;
export const SizeZero = SizesModule.SizeZero;
export const Square = SizesModule.Square;
export const PortraitShape = SizesModule.PortraitShape;
export const LandscapeShape = SizesModule.LandscapeShape;
export const Band = SizesModule.Band;
export const ThinBand = SizesModule.ThinBand;
export const displaySize = SizesModule.displaySize;
export const GapBases = SizesModule.GapBases;
export const SquareBases = SizesModule.SquareBases;
export const PicsBases = SizesModule.PicsBases;
export const LandscapePicsBases = SizesModule.LandscapePicsBases;
export const BandBases = SizesModule.BandBases;
export const useHighScreen = SizesModule.useHighScreen;
export const useMiniScreen = SizesModule.useMiniScreen;
export const useTinyScreen = SizesModule.useTinyScreen;
export const useSmallScreen = SizesModule.useSmallScreen;
export const useNormalScreen = SizesModule.useNormalScreen;
export const useLargeScreen = SizesModule.useLargeScreen;
export const useHugeScreen = SizesModule.useHugeScreen;
export const useScreenWidth = SizesModule.useScreenWidth;
export const ModalWindowFix = ModalModule.ModalWindowFix;
export const ModalWindowFluid = ModalModule.ModalWindowFluid;

export {
  lorem,
  Integer,
  Float,
  Enum,
  TSTricks,
  SizedContent,
  SiteLayout,
  Menu,
  HSV,
  HSVColor,
  BiColor,
  ModalFormItem,
  ModalFormGroup,
  useArrayState,
  useLocalStorage,
  useTextLoader,
  Button,
  RadioButton,
  CheckBoxButton,
  Toggle,
  MainForm,
  useMemberPageLayoutSizes,
};
