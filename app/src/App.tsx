import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import './App.scss';
import { SiteLayout } from './pack/index';
import MainHeader from './guide/Components/Organisms/Header';
import Brand from './guide/Components/Organisms/Brand';
import Footer from './guide/Components/Organisms/Footer';
import { PageContent, PageHeader } from './guide/PageProvider';

function App() {
  const header = () => <MainHeader />;
  const header2 = () => <PageHeader />;
  const brand = () => <Brand />;
  const footer = () => <Footer />;

  const main = () => <PageContent />;

  return (
    <Router>
      <SiteLayout header={header} header2={header2} brand={brand} footer={footer} main={main} />
    </Router>
  );
}

export default App;
