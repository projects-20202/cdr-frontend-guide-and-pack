import * as React from 'react';
import { Organism, useScreenWidth } from '../../../pack/index';

const Brand: () => JSX.Element = () => {
  const screen = useScreenWidth();
  return (
    <Organism>
      <h1>COD. GUIDE V2 -- SCREEN : {screen}</h1>
    </Organism>
  );
};

export default Brand;
