import * as React from 'react';
import { Organism } from '../../../pack/index';

const Footer: () => JSX.Element = () => {
  return (
    <Organism>
      <h1>COD. END V2 !</h1>
    </Organism>
  );
};

export default Footer;
