import * as React from 'react';
import { CheckBoxButton, InputNumber, ModalFormGroup, ModalFormItemProps, Organism } from '../../../../pack/index';
import { FilteringControllerContract } from '../../../Controllers/Filtering/FilteringController';
import ModalForm from '../../../../pack/Layouts/Components/ModalForm';
import { CriteriaEValue, eValues } from '../../../Models/FilteringModels';

const FilteringDEModal: (props: { filteringController: FilteringControllerContract }) => JSX.Element = (props) => {
  const filteringController = props.filteringController;
  const allESelected =
    props.filteringController.currentFilter.e.v1 &&
    props.filteringController.currentFilter.e.v2 &&
    props.filteringController.currentFilter.e.v3 &&
    props.filteringController.currentFilter.e.v4 &&
    props.filteringController.currentFilter.e.v5 &&
    props.filteringController.currentFilter.e.v6;

  const dValueToFormItem: () => ModalFormItemProps[] = () => {
    const min = filteringController.currentFilter.d ? filteringController.currentFilter.d.min : 0;
    const max = filteringController.currentFilter.d ? filteringController.currentFilter.d.max : 1000;
    const updateMin = (newMin: number) =>
      filteringController.setFilter.setD({
        min: newMin,
        max,
      });
    const updateMax = (newMax: number) =>
      filteringController.setFilter.setD({
        min,
        max: newMax,
      });
    return [
      { content: <InputNumber value={min} mapChange={updateMin} />, control: 'min' },
      { content: <InputNumber value={max} mapChange={updateMax} />, control: 'max' },
    ];
  };

  const setAllE: () => JSX.Element = () => {
    const active = allESelected;
    const reset = () => {
      filteringController.setFilter.setE(
        active
          ? { v1: false, v2: false, v3: false, v4: false, v5: false, v6: false }
          : { v1: true, v2: true, v3: true, v4: true, v5: true, v6: true },
      );
    };
    return <CheckBoxButton active={active} toggle={reset} />;
  };

  const eValueToFormItem: (value: CriteriaEValue) => ModalFormItemProps = (value) => {
    const active = filteringController.currentFilter.e[value];
    const update = () => {
      const copyE = { ...filteringController.currentFilter.e };
      copyE[value] = !active;
      filteringController.setFilter.setE(copyE);
    };
    const control = <CheckBoxButton active={active} toggle={update} />;
    const content = value;
    return { content, control };
  };

  return (
    <Organism>
      <h1>Filtering D - E</h1>
      <ModalForm>
        <ModalFormGroup title={'D: Min/Max'} titleControl={'>'} items={dValueToFormItem()} />
        <ModalFormGroup title={'E. Multiple choices'} titleControl={setAllE()} items={eValues.map(eValueToFormItem)} />
      </ModalForm>
      <pre>{JSON.stringify(filteringController.currentFilter, null, 2)}</pre>
    </Organism>
  );
};

export default FilteringDEModal;
