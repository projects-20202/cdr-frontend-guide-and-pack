import * as React from 'react';
import { InputText, ModalFormGroup, ModalFormItemProps, Organism, RadioButton } from '../../../../pack/index';
import { FilteringControllerContract } from '../../../Controllers/Filtering/FilteringController';
import ModalForm from '../../../../pack/Layouts/Components/ModalForm';
import { aValues, CriteriaAValue } from '../../../Models/FilteringModels';

const FilteringACModal: (props: { filteringController: FilteringControllerContract }) => JSX.Element = (props) => {
  const filteringController = props.filteringController;

  const aValueToFormItem: (value: CriteriaAValue) => ModalFormItemProps = (value) => {
    const active = filteringController.currentFilter.a === value;
    const control = (
      <RadioButton active={active} toggle={() => filteringController.setFilter.setA(active ? null : value)} />
    );
    const content = value;
    return { content, control };
  };

  const bValueToFormItem: () => ModalFormItemProps = () => {
    const active = filteringController.currentFilter.b;
    const control = <RadioButton active={active} toggle={() => filteringController.setFilter.setB(!active)} />;
    const content = 'Is B Filter Set';
    return { content, control };
  };

  const cValueToFormItem: () => ModalFormItemProps = () => {
    const cValue = filteringController.currentFilter.c;
    const content = (
      <InputText value={cValue} mapChange={filteringController.setFilter.setC} placeHolder={'type you'} />
    );
    const control = <></>; // <div className="icon-cloth"/>
    return { content, control };
  };

  return (
    <Organism>
      <h1>Filtering A - C</h1>
      <ModalForm>
        <ModalFormGroup title="A. One value among Several" items={aValues.map(aValueToFormItem)} />
        <ModalFormGroup title={'B. Boolean'} titleControl={'>'} items={[bValueToFormItem()]} />
        <ModalFormGroup title={'C. Free string'} items={[cValueToFormItem()]} />
      </ModalForm>
      <pre>{JSON.stringify(filteringController.currentFilter, null, 2)}</pre>
    </Organism>
  );
};

export default FilteringACModal;
