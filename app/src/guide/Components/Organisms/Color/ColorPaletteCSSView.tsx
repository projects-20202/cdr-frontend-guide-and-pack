import * as React from 'react';
import { HSV, HSVColor, Organism } from '../../../../pack/index';
import './colorHuesTemplate.scss';

type ColorPaletteCSSViewProps = {
  hue: number;
  hueIndex: number;
  sat: number[];
  val: number[];
  hueClasses: string[];
  satClasses: string[];
  valClasses: string[];
};
const ColorPaletteCSSView: (props: ColorPaletteCSSViewProps) => JSX.Element = (props) => {
  const hueBeautify = (i: number) => props.hueClasses[i];

  const valueBeautify = (i: number) => props.valClasses[i];

  const satBeautify = (i: number) => props.satClasses[i];

  const h = props.hue;

  const grid = () =>
    props.sat.map((s, is) => {
      return props.val.map((v, iv) => {
        const color = new HSV(h, s, v);
        return (
          <HSVColor color={color} key={h * 1000 + is * 100 + iv}>
            <pre>
              .{hueBeautify(props.hueIndex)}-{satBeautify(is)}-{valueBeautify(iv)} {'{\n'}
              {'  /*'}H:{color.h} S:{(color.s * 100).toFixed(0)} V:{(color.v * 100).toFixed(0)}
              {'*/\n'}
              {'  '}color: {color.complementaryColor().toHex()}; stroke: {color.complementaryColor().toHex()}; {'\n'}
              {'  '}background-color: {color.toHex()}; fill: {color.toHex()}; {'\n'}
              {'}'}
            </pre>
          </HSVColor>
        );
      });
    });

  return (
    <Organism>
      <div className="color-hues-palette-large-grid">{grid()}</div>
    </Organism>
  );
};

export default ColorPaletteCSSView;
