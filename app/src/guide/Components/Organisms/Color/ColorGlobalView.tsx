import * as React from 'react';
import { HSV, HSVColor, Organism } from '../../../../pack/index';
import './colorGlobalTemplate.scss';

const ColorGlobalView: () => JSX.Element = () => {
  const values: HSV[] = [];
  const stepH = 10;
  const stepSV = 0.1;

  for (let h = 0; h < 360; h += stepH) {
    for (let s = 0; s < 1; s += stepSV) {
      for (let v = 0; v < 1; v += stepSV) {
        values.push(new HSV(h, s, v));
      }
    }
  }
  return (
    <Organism>
      <div className="color-picker-grid">
        {values.map((color, ic) => (
          <HSVColor color={color} key={ic}>
            {color.h} . {(color.s * 10).toFixed(0)} .{(color.v * 10).toFixed(0)}
          </HSVColor>
        ))}
      </div>
    </Organism>
  );
};

export default ColorGlobalView;
