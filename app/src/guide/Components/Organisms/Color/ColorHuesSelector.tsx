import * as React from 'react';
import { HSV, HSVColor, Organism, SizedContent, SquareBases } from '../../../../pack/index';
import './colorHuesTemplate.scss';

type ColorHuesSelectorProps = {
  stepH: number;
  hue: number;
  setHue: (newHue: number) => void;
};
const ColorHuesSelector: (props: ColorHuesSelectorProps) => JSX.Element = (props) => {
  const values = [];
  for (let h = 0; h < 360; h += props.stepH) {
    const color = new HSV(h, 0.8, 1);
    values.push(
      <SizedContent {...SquareBases.MINI} key={h}>
        <HSVColor color={color} className={h === props.hue ? 'active' : 'inactive'} onClick={() => props.setHue(h)}>
          {color.h}
        </HSVColor>
      </SizedContent>,
    );
  }
  return (
    <Organism>
      <div className="color-hue-selector">{values}</div>
    </Organism>
  );
};

export default ColorHuesSelector;
