import * as React from 'react';
import { HSV, HSVColor, Organism } from '../../../../pack/index';
import './colorHuesTemplate.scss';

type ColorPaletteViewProps = {
  hue: number;
  sat: number[];
  val: number[];
};
const ColorPaletteView: (props: ColorPaletteViewProps) => JSX.Element = (props) => {
  const h = props.hue;
  const grid = () =>
    props.sat.map((s, is) => {
      return props.val.map((v, iv) => {
        const color = new HSV(h, s, v);
        return (
          <HSVColor color={color} key={h * 1000 + is * 100 + iv}>
            {(color.s * 100).toFixed(0)} .{(color.v * 100).toFixed(0)}
          </HSVColor>
        );
      });
    });

  return (
    <Organism>
      <div className="color-hues-palette-grid">{grid()}</div>
    </Organism>
  );
};

export default ColorPaletteView;
