import * as React from 'react';
import { HSV, Organism } from '../../../../pack/index';
import './colorHuesTemplate.scss';

type ColorPaletteCSSViewProps = {
  hue: number;
  hueIndex: number;
  sat: number[];
  val: number[];
  hueClasses: string[];
  satClasses: string[];
  valClasses: string[];
};
const ColorPaletteCSS: (props: ColorPaletteCSSViewProps) => JSX.Element = (props) => {
  const hueBeautify = (i: number) => props.hueClasses[i];

  const valueBeautify = (i: number) => props.valClasses[i];

  const satBeautify = (i: number) => props.satClasses[i];

  const h = props.hue;

  const grid = () =>
    props.sat.map((s, is) => {
      return props.val.map((v, iv) => {
        const color = new HSV(h, s, v);
        return (
          <React.Fragment key={is * 100 + iv}>
            .{hueBeautify(props.hueIndex)}-{satBeautify(is)}-{valueBeautify(iv)} {'{\n'}
            {'  '}color: {color.complementaryColor().toHex()}; stroke: {color.complementaryColor().toHex()}; {'\n'}
            {'  '}background-color: {color.toHex()}; fill: {color.toHex()}; {'\n'}
            {'}\n'}
          </React.Fragment>
        );
      });
    });

  return (
    <Organism>
      <pre>{grid()}</pre>
    </Organism>
  );
};

export default ColorPaletteCSS;
