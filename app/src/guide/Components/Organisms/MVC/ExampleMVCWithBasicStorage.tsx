import { useExampleStorageController } from '../../../Controllers/States/ExampleStorageController';
import ExampleView from '../../../Controllers/States/ExampleView';

const ExampleMVCWithBasicStorage: () => JSX.Element = () => {
  const exampleController = useExampleStorageController();
  return ExampleView(exampleController);
};

export default ExampleMVCWithBasicStorage;
