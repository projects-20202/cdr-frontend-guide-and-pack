import * as React from 'react';
import { useRouteMatch } from 'react-router-dom';
import {
  MainForm,
  Organism,
  useRouteGoBack,
  useRouteGoTo,
  useRouteMatchExact,
  useRouteMatchParams,
} from '../../../../pack/index';

const ExampleRouteNavigation: () => JSX.Element = () => {
  const go = useRouteGoTo();
  const back = useRouteGoBack();

  const baseMatch = useRouteMatch('/mvc/navigation');
  const currentMatch = useRouteMatch('/mvc/navigation/:voice/:param/:otherParam');

  const matchBasics = {
    current: useRouteMatch(),
    route1: useRouteMatch('/mvc/navigation/route1'),
    route2: useRouteMatch('/mvc/navigation/route2'),
    route3: useRouteMatch('/mvc/navigation/route3'),
    route4: useRouteMatch('/mvc/navigation/route4'),
    route5: useRouteMatch('/mvc/navigation/route5'),
    'route1.1': useRouteMatch('/mvc/navigation/route1/:firstParam'),
    'route2.1': useRouteMatch('/mvc/navigation/route2/:firstParam'),
    'route3.1': useRouteMatch('/mvc/navigation/route3/:firstParam'),
    'route4.1': useRouteMatch('/mvc/navigation/route4/:firstParam'),
    'route5.1': useRouteMatch('/mvc/navigation/route5/:firstParam'),
  };
  const match = {
    route0: useRouteMatchExact('/mvc/navigation/route0/:param/:otherParam'),
    route1: useRouteMatchExact('/mvc/navigation/route1/:param/:otherParam'),
    route2: useRouteMatchExact('/mvc/navigation/route2/:param/:otherParam'),
    route3: useRouteMatchExact('/mvc/navigation/route3/:param/:otherParam'),
    route4: useRouteMatchExact('/mvc/navigation/route4/:param/:otherParam'),
    route5: useRouteMatchExact('/mvc/navigation/route5/:param/:otherParam'),
  };
  const matchParams = {
    route0: useRouteMatchParams('/mvc/navigation/route0/:param/:otherParam'),
    route1: useRouteMatchParams('/mvc/navigation/route1/:param/:otherParam'),
    route2: useRouteMatchParams('/mvc/navigation/route2/:param/:otherParam'),
    route3: useRouteMatchParams('/mvc/navigation/route3/:param/:otherParam'),
    route4: useRouteMatchParams('/mvc/navigation/route4/:param/:otherParam'),
    route5: useRouteMatchParams('/mvc/navigation/route5/:param/:otherParam'),
  };

  const items = [
    <MainForm>
      <h2>Go To</h2>
      <button onClick={go('/mvc/navigation/route0/firstParamValue/secondParamValue')}>0</button>
      <button onClick={go('/mvc/navigation/route1/firstParamValue/secondParamValue')}>1</button>
      <button onClick={go('/mvc/navigation/route2/firstParamValue/secondParamValue')}>2</button>
      <button onClick={go('/mvc/navigation/route3/firstParamValue/secondParamValue')}>3</button>
      <button onClick={go('/mvc/navigation/route4/firstParamValue/secondParamValue')}>4</button>
      <button onClick={go('/mvc/navigation/route5/firstParamValue/secondParamValue')}>5</button>
      <h2>Go Back</h2>
      <button onClick={back}>BACK</button>
    </MainForm>,
    <>
      <h2>Using exact match helper</h2>
      <pre>{JSON.stringify(match, null, 2)}</pre>
      <h2>Using match params helper</h2>
      <pre>{JSON.stringify(matchParams, null, 2)}</pre>
      <h2>Using basic dom router</h2>
      <pre>{JSON.stringify(matchBasics, null, 2)}</pre>
    </>,
  ];
  return (
    <Organism>
      <h1>This is Route Navigation example</h1>
      <h2>
        URL = {baseMatch?.url} -- {currentMatch?.url}
      </h2>
      <h2>
        PATH = {baseMatch?.path} -- {currentMatch?.path}
      </h2>
      {items}
    </Organism>
  );
};

export default ExampleRouteNavigation;
