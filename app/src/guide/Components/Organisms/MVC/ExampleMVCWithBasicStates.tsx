import { useExampleController } from '../../../Controllers/States/ExampleController';
import ExampleView from '../../../Controllers/States/ExampleView';

const ExampleMVCWithBasicStates: () => JSX.Element = () => {
  const exampleController = useExampleController();
  return ExampleView(exampleController);
};

export default ExampleMVCWithBasicStates;
