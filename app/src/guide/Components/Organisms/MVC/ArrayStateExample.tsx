import * as React from 'react';
import { useState } from 'react';
import './mvc-array.scss';
import {
  ArrayState,
  ChildrenProps,
  ColorHue,
  ColorSat,
  ColorValue,
  InputNumber,
  MainForm,
  Organism,
  SizedContent,
  SquareBases,
  useArrayState,
} from '../../../../pack/index';

const hues: ColorHue[] = ['alpha', 'beta', 'gamma', 'delta', 'omega'];
const sat: ColorSat[] = ['fader', 'fade', 'pastel', 'bright', 'brighter'];
const val: ColorValue[] = ['darker', 'dark', 'dusky', 'light'];

const randValue: () => number = () => Math.ceil(Math.random() * 100);
const randValues: () => number[] = () => {
  const count = Math.ceil(Math.random() * 20);
  const values = [];
  for (let i = 0; i < count; i++) {
    values.push(randValue());
  }
  return values;
};

const ColorCompo = (h: ColorHue, s: ColorSat, v: ColorValue) => () => {
  return <SizedContent {...SquareBases.MINI} className={'color-' + h + '-' + s + '-' + v} />;
};

const coloredNumberView = () => {
  const allComponents: ((props: ChildrenProps) => JSX.Element)[] = [];
  hues.map((h) => {
    return sat.map((s) => {
      return val.map((v) => {
        allComponents.push(ColorCompo(h, s, v));
        return '';
      });
    });
  });
  return allComponents;
};
const components: ((props: ChildrenProps) => JSX.Element)[] = coloredNumberView(); // 100 components

const ArrayStateExample: () => JSX.Element = () => {
  const allValues: ArrayState<number> = useArrayState<number>();

  const items = allValues.data.map((n: number, i: number) => {
    const Compo = components[n % 100];
    return (
      <SizedContent w={50} h={50} key={i} className={'mvc-bordered'}>
        <Compo />
      </SizedContent>
    );
  });

  const [valueToRemove, setValueToRemove] = useState<number>(0);
  const [valueToAdd, setValueToAdd] = useState<number>(0);

  const content = [
    <div className="section-container">
      <MainForm>
        <h2>Edit your array here</h2>
        <button onClick={() => allValues.add(randValue())}>ADD RANDOM</button>
        <InputNumber id={'add-val'} value={valueToAdd} label={'Add Value'} mapChange={setValueToAdd} />
        <button onClick={() => allValues.add(valueToAdd)}>ADD VALUE</button>
        <button onClick={() => allValues.pop()}>POP</button>
        <InputNumber id={'remove-val'} value={valueToRemove} label={'Remove Value'} mapChange={setValueToRemove} />
        <button onClick={() => allValues.remove(valueToRemove)}>REMOVE</button>
        <button onClick={() => allValues.unique()}>UNIQUE</button>
        <button onClick={() => allValues.update([])}>SET TO EMPTY</button>
        <button onClick={() => allValues.update(randValues())}>RESET WITH RANDOM VALUES</button>
      </MainForm>
    </div>,
    <div className="section-container">
      <h2>See here my {allValues.data.length} numbers</h2>
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>{items}</div>
    </div>,
  ];
  return (
    <Organism>
      <div className="section-container">
        <h1>This is an example of arrayState usage</h1>
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>{content}</div>
        <pre>{JSON.stringify(allValues)}</pre>
      </div>
    </Organism>
  );
};

export default ArrayStateExample;
