import * as React from 'react';
import { Organism, useTextLoader } from '../../../../pack/index';
// eslint-disable-next-line import/no-webpack-loader-syntax
import example01 from '!file-loader!./example01.md';
// eslint-disable-next-line import/no-webpack-loader-syntax
import example02 from '!raw-loader!./example02.md';

const ExampleTextLoader: () => JSX.Element = () => {
  const loadedExample1Content = useTextLoader(example01 as Request | string);

  return (
    <Organism>
      <h1>With File Loader</h1>
      <pre>{loadedExample1Content}</pre>
      <h1>With Row Loader</h1>
      <pre>{example02}</pre>
    </Organism>
  );
};

export default ExampleTextLoader;
