import { BiColor } from '../../../../pack/index';
import * as React from 'react';

const BiColorExample: (props: {
  ratio: 50 | 75 | 90;
  text1: (string | JSX.Element)[];
  text2: (string | JSX.Element)[];
}) => JSX.Element = (props) => {
  const [ratio, text1, text2] = [props.ratio, props.text1, props.text2];

  return (
    <>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <BiColor
          w={50}
          h={30}
          ratio={ratio}
          firstColorClass="color-alpha-fader-dusky"
          secondColorClass="color-alpha-bright-light"
          firstContent={<div>{text1}</div>}
          secondContent={<div>{text2}</div>}
        />
        <BiColor
          w={100}
          h={100}
          ratio={ratio}
          firstColorClass="color-beta-fader-dusky"
          secondColorClass="color-beta-bright-light"
          firstContent={<div>{text1}</div>}
          secondContent={<div>{text2}</div>}
        />
      </div>
      <BiColor
        w={200}
        h={100}
        ratio={ratio}
        firstColorClass="color-gamma-fader-dusky"
        secondColorClass="color-gamma-bright-light"
        firstContent={<div>{text1}</div>}
        secondContent={<div>{text2}</div>}
      />
      <BiColor
        w={600}
        h={300}
        ratio={ratio}
        firstColorClass="color-delta-fader-dusky"
        secondColorClass="color-delta-bright-light"
        firstContent={<div>{text1}</div>}
        secondContent={<div>{text2}</div>}
      />
      <BiColor
        w={600}
        h={200}
        ratio={ratio}
        firstColorClass="color-omega-fader-dusky"
        secondColorClass="color-omega-bright-light"
        firstContent={<div>{text1}</div>}
        secondContent={<div>{text2}</div>}
      />
    </>
  );
};

export default BiColorExample;
