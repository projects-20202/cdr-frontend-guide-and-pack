import * as React from 'react';
import { Menu, Organism } from '../../../pack/index';

const MainHeader: () => JSX.Element = () => {
  const items = [
    { to: '/colors', label: 'Colors' },
    { to: '/layouts', label: 'Layouts' },
    { to: '/icons', label: 'Icons' },
    { to: '/forms', label: 'Forms' },
    { to: '/mvc', label: 'MVC' },
    { to: '/filtering', label: 'Filtering' },
    { to: '/themes', label: 'THEMES' },
  ];
  return (
    <Organism>
      <Menu items={items} />
    </Organism>
  );
};

export default MainHeader;
