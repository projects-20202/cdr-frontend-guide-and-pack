# The 'pack' 

In directory pack, you'll find the source package, for npm compilation and publishing on the registry.

Never edit the src, lib or styles directories. They are built automatically. Instead, edit the `app/src/pack` directory.

- Run the script `pack-install.sh` to install the package dependencies
- Build the package using ths script `pack-build.sh`.
- Publish the package with one of the scripts `pack-patch.sh`, `pack-minor.sh` or `pack-major.sh`

# The 'guide'

In the directory *app*, we define a full React application that can be executed to see the package in action and demonstrate how to use it.

In the *app/src/guide* directory, we define the pages of this guide. 

In the *app/src/pack* directory, we define the bases components and definitions that must be published in the package.

WARNING : *app/src/pack* MUST be totally autonomous and should never depend on components defined in the *app/src/guide* directory.

- Run the script `guide-install.sh` to install the guide dependencies
- Build and deploy the guide locally using the script `guide-npm-start.sh`
- PS : before committing, please use the `guide-npm-format.sh` script to improve code quality. 

View the result [locally](http://cdr-guide.dev.local) 

# Development process

Work in the *app* directory to create new components (in `app/src/pack`) and visualize the result using those components in the `app/src/guide`.

When you are ready, build and publish the package, and commit your result. Then try to use it in a real project, if it's ok start again with a new feature.

Please be very careful on your documentation quality. The guide must help other developers to use your components with ease.
