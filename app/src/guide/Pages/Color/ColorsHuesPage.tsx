import * as React from 'react';
import { useState } from 'react';
import { Organism, Page } from '../../../pack/index';
import ColorHuesSelector from '../../Components/Organisms/Color/ColorHuesSelector';
import ColorPaletteView from '../../Components/Organisms/Color/ColorPaletteView';

const ColorHuesPage: () => JSX.Element = () => {
  const stepH = 5;
  const sat = [0.02, 0.1, 0.2, 0.6, 0.8];
  const val = [0.15, 0.2, 0.3, 1];

  const [hue, setHue] = useState<number>(0);

  return (
    <Page>
      <div className="section-container">
        <h1>Hello ColorHuesPage</h1>
        <Organism>
          <h2>Select Hue</h2>
          <ColorHuesSelector hue={hue} setHue={setHue} stepH={stepH} />
          <h2>View your palette</h2>
          <ColorPaletteView hue={hue} sat={sat} val={val} />
        </Organism>
        );
      </div>
    </Page>
  );
};

export default ColorHuesPage;
