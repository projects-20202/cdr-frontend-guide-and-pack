import * as React from 'react';
import { Page } from '../../../pack/index';
import ColorGlobalView from '../../Components/Organisms/Color/ColorGlobalView';

const ColorGlobalPage: () => JSX.Element = () => {
  return (
    <Page>
      <div className="section-container">
        <h1>Hello ColorGlobalPage</h1>
        <ColorGlobalView />
      </div>
    </Page>
  );
};

export default ColorGlobalPage;
