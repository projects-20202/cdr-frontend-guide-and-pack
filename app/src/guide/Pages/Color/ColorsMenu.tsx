const colorMenuItems = [
  {
    to: '/colors/guide',
    label: 'Guide',
  },
  {
    to: '/colors/hues',
    label: 'Hues-Helper',
  },
  {
    to: '/colors/global',
    label: 'Global-Helper',
  },
];

export default colorMenuItems;
