import * as React from 'react';
import { Page } from '../../../pack/index';

const ColorMainPage: () => JSX.Element = () => {
  return (
    <Page>
      <div className="section-container">
        <h1>Hello ColorMainPage</h1>
      </div>
    </Page>
  );
};

export default ColorMainPage;
