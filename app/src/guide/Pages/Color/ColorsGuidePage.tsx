import * as React from 'react';
import { ColorHue, ColorSat, ColorValue, Page } from '../../../pack/index';
import ColorPaletteCSSView from '../../Components/Organisms/Color/ColorPaletteCSSView';
import ColorPaletteView from '../../Components/Organisms/Color/ColorPaletteView';
import ColorPaletteCSS from '../../Components/Organisms/Color/ColorPaletteCSS';
import './ColorGuideTemplate.scss';

const ColorGuidePage: () => JSX.Element = () => {
  const vHues = [55, 120, 185, 250, 315];
  const hues: ColorHue[] = ['alpha', 'beta', 'gamma', 'delta', 'omega'];

  const sat: ColorSat[] = ['fader', 'fade', 'pastel', 'bright', 'brighter'];
  const vSats = [0.02, 0.1, 0.2, 0.6, 0.8];

  const val: ColorValue[] = ['darker', 'dark', 'dusky', 'light'];
  const vValues = [0.15, 0.2, 0.3, 1];

  const previews = hues.map((h, i) => {
    return (
      <div key={i}>
        <h3 className="center">{hues[i]}</h3>
        <ColorPaletteView hue={vHues[i]} sat={vSats} val={vValues} />
      </div>
    );
  });

  const palettes = hues.map((h, i) => {
    return (
      <React.Fragment key={i}>
        <h3>{hues[i]}</h3>
        <ColorPaletteCSSView
          hue={vHues[i]}
          hueIndex={i}
          sat={vSats}
          val={vValues}
          hueClasses={hues}
          satClasses={sat}
          valClasses={val}
        />
      </React.Fragment>
    );
  });

  const cssSource = hues.map((h, i) => {
    return (
      <React.Fragment key={i}>
        <ColorPaletteCSS
          hue={vHues[i]}
          hueIndex={i}
          sat={vSats}
          val={vValues}
          hueClasses={hues}
          satClasses={sat}
          valClasses={val}
        />
      </React.Fragment>
    );
  });
  return (
    <Page>
      <div className="section-container">
        <h1>Hello ColorGuidePage</h1>
        <h2>Preview</h2>
        <div className="preview-palettes-grid">{previews}</div>
        <h2>View CSS Classes</h2>
        {palettes}
        <h2>View CSS Source Code</h2>
        {cssSource}
      </div>
    </Page>
  );
};

export default ColorGuidePage;
