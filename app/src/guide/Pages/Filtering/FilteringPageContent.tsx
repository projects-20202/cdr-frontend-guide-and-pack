import * as React from 'react';
import { BandBases, ModalWindowFix, Page, SizedContent, useRouteMatchExact } from '../../../pack/index';
import { useFilteringController } from '../../Controllers/Filtering/FilteringController';
import FilteringACModal from '../../Components/Organisms/Filtering/FilteringACModal';
import FilteringDEModal from '../../Components/Organisms/Filtering/FilteringDEModal';
import { useHistory } from 'react-router-dom';

const FilteringPageContent: () => JSX.Element = () => {
  const filteringController = useFilteringController();
  const currentlyFilteringAC = useRouteMatchExact('/filtering/criteria/AC');
  const currentlyFilteringDE = useRouteMatchExact('/filtering/criteria/DE');
  const history = useHistory();

  const results = filteringController.getResults();
  const closeModal = () => history.push('/filtering');

  return (
    <Page>
      {currentlyFilteringAC && (
        <ModalWindowFix onClose={closeModal}>
          <FilteringACModal filteringController={filteringController} />
        </ModalWindowFix>
      )}
      {currentlyFilteringDE && (
        <ModalWindowFix onClose={closeModal}>
          <FilteringDEModal filteringController={filteringController} />
        </ModalWindowFix>
      )}
      <div className="section-container" onClick={closeModal}>
        <h1>Filtering Page</h1>
        <SizedContent h={BandBases.NORMAL.h} className="color-alpha-pastel-light center">
          <h2>HERE THE RESULTS</h2>
        </SizedContent>
        <pre>{JSON.stringify(results, null, 2)}</pre>
      </div>
    </Page>
  );
};

export default FilteringPageContent;
