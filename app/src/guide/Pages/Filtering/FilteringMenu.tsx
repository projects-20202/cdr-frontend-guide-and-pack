import { MenuItemProps } from '../../../pack/index';
import { useRouteMatch } from 'react-router-dom';

const filteringMenuItems = [
  {
    to: '/filtering/criteria/AC',
    label: 'Criteria A-C',
  },
  {
    to: '/filtering/criteria/DE',
    label: 'Criteria D-E',
  },
];

const filteringMenuItemsAC = [
  {
    to: '/filtering',
    label: 'Criteria A-C',
  },
  {
    to: '/filtering/criteria/DE',
    label: 'Criteria D-E',
  },
];

const filteringMenuItemsDE = [
  {
    to: '/filtering/criteria/AC',
    label: 'Criteria A-C',
  },
  {
    to: '/filtering',
    label: 'Criteria D-E',
  },
];

const useFilteringMenu: () => MenuItemProps[] = () => {
  const filteringAC = useRouteMatch('/filtering/criteria/AC');
  const filteringDE = useRouteMatch('/filtering/criteria/DE');

  if (filteringAC) {
    return filteringMenuItemsAC;
  }
  if (filteringDE) {
    return filteringMenuItemsDE;
  }
  return filteringMenuItems;
};

export default useFilteringMenu;
