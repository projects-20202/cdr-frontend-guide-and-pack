import * as React from 'react';
import { Page } from '../../../pack/index';

const IconMainPage: () => JSX.Element = () => {
  return (
    <Page>
      <div className="section-container">
        <h1>Hello IconMainPage</h1>
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          <span className="icon-cloth" />
        </div>
      </div>
    </Page>
  );
};

export default IconMainPage;
