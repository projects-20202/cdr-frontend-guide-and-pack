import * as React from 'react';
import { BandBases, LandscapePicsBases, Page, PicsBases, SizedContent, SquareBases } from '../../../pack/index';

const LayoutsSizesPage: () => JSX.Element = () => {
  return (
    <Page>
      <div className="section-container">
        <h1>Hello LayoutsSizesPage</h1>
        <h2>Square</h2>
        <SizedContent {...SquareBases.NORMAL} className="center color-alpha-pastel-light">
          SquareBases.NORMAL
        </SizedContent>
        <h2>Portrait</h2>
        <SizedContent {...PicsBases.NORMAL} className="center color-beta-pastel-light">
          PicsBases.NORMAL
        </SizedContent>
        <h2>Landscape</h2>
        <SizedContent {...LandscapePicsBases.NORMAL} className="center color-gamma-pastel-light">
          LandscapePicsBases.NORMAL
        </SizedContent>
        <h2>Band</h2>
        <SizedContent {...BandBases.NORMAL} className="center color-delta-pastel-light">
          BandBases.NORMAL
        </SizedContent>
        <h2>Predefined Sizes</h2>
        <div style={{ display: 'flex', flexWrap: 'wrap' }} className="color-alpha-pastel-dusky section-container">
          <SizedContent {...SquareBases.MINI} className="center color-alpha-pastel-light">
            MINI
          </SizedContent>
          <SizedContent {...SquareBases.TINY} className="center color-beta-pastel-light">
            TINY
          </SizedContent>
          <SizedContent {...SquareBases.SMALL} className="center color-gamma-pastel-light">
            SMALL
          </SizedContent>
          <SizedContent {...SquareBases.NORMAL} className="center color-delta-pastel-light">
            NORMAL
          </SizedContent>
          <SizedContent {...SquareBases.PLUS} className="center color-omega-pastel-light">
            PLUS
          </SizedContent>
          <SizedContent {...SquareBases.LARGE} className="center color-delta-pastel-light">
            LARGE
          </SizedContent>
          <SizedContent {...SquareBases.HUGE} className="center color-gamma-pastel-light">
            HUGE
          </SizedContent>
        </div>
      </div>
    </Page>
  );
};

export default LayoutsSizesPage;
