import * as React from 'react';
import { lorem, Page, SizedContent, SquareBases } from '../../../pack/index';
import BiColorExample from '../../Components/Organisms/BiColor/BiColorExample';
import './layoutsContainer.scss';

const LayoutsContainersPage: () => JSX.Element = () => {
  const genText = () => {
    const words: (string | JSX.Element)[] = [];
    const nLines = Math.ceil(Math.random() * 4);
    for (let i = 0; i < nLines; i++) {
      words.push(lorem.generateWords(Math.ceil(Math.random() * 10)));
      words.push(<br />);
    }
    return words;
  };
  const text1 = genText();
  const text2 = genText();

  return (
    <Page>
      <div className="section-container">
        <h1>Hello LayoutsContainersPage</h1>
        <h2>Section Container</h2>
        <SizedContent {...SquareBases.PLUS} className="section-container color-beta-pastel-dusky">
          <div className="color-beta-pastel-light center maximize">
            {text1}
            <br />
            {text2}
            <br />
            {text1}
            <br />
            {text2}
            <br />
            {text1}
            <br />
            {text2}
            <br />
          </div>
        </SizedContent>
        <h2>Little Container</h2>
        <SizedContent {...SquareBases.PLUS} className="little-container color-beta-pastel-dusky">
          <div className="color-beta-pastel-light maximize">
            {text1}
            <br />
            {text2}
            <br />
            {text1}
            <br />
            {text2}
            <br />
            {text1}
            <br />
            {text2}
            <br />
          </div>
        </SizedContent>
        <h2>Bi-Color</h2>
        <div className="bi-color-example-container">
          <BiColorExample ratio={50} text1={text1} text2={text2} />
          <BiColorExample ratio={75} text1={text1} text2={text2} />
          <BiColorExample ratio={90} text1={text1} text2={text2} />
        </div>
      </div>
    </Page>
  );
};

export default LayoutsContainersPage;
