import * as React from 'react';
import { Page, SizedContent, SquareBases } from '../../../pack/index';
import './layoutsGaps.scss';

const LayoutsGapsPage: () => JSX.Element = () => {
  return (
    <Page>
      <div className="section-container">
        <h1>Hello LayoutsGapsPage</h1>
        <h2>Padding Basic</h2>
        <SizedContent {...SquareBases.PLUS} className="section-container color-beta-pastel-dusky">
          <div className="color-beta-pastel-light center maximize">
            Normal Padding <br /> Center <br /> Maximize
          </div>
        </SizedContent>
        <h2>All Paddings</h2>
        <div id="layout-gaps-page" className="color-alpha-pastel-light">
          <SizedContent {...SquareBases.SMALL} className="gap-mini color-alpha-pastel-dusky">
            <div className="color-alpha-pastel-light center maximize">MINI</div>
          </SizedContent>
          <SizedContent {...SquareBases.SMALL} className="gap-tiny color-beta-pastel-dusky">
            <div className="color-beta-pastel-light center maximize">TINY</div>
          </SizedContent>
          <SizedContent {...SquareBases.SMALL} className="gap-small color-gamma-pastel-dusky">
            <div className="color-gamma-pastel-light center maximize">SMALL</div>
          </SizedContent>
          <SizedContent {...SquareBases.SMALL} className="gap-normal color-delta-pastel-dusky">
            <div className="color-delta-pastel-light center maximize">NORMAL</div>
          </SizedContent>
          <SizedContent {...SquareBases.SMALL} className="gap-plus color-omega-pastel-dusky">
            <div className="color-omega-pastel-light center maximize">PLUS</div>
          </SizedContent>
          <SizedContent {...SquareBases.SMALL} className="gap-large color-delta-pastel-dusky">
            <div className="color-delta-pastel-light center maximize">LARGE</div>
          </SizedContent>
          <SizedContent {...SquareBases.SMALL} className="gap-huge color-gamma-pastel-dusky">
            <div className="color-gamma-pastel-light center maximize">HUGE</div>
          </SizedContent>
        </div>
      </div>
    </Page>
  );
};

export default LayoutsGapsPage;
