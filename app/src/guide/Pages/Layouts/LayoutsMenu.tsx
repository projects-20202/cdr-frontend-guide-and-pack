const layoutsMenuItems = [
  {
    to: '/layouts/gaps',
    label: 'Gaps',
  },
  {
    to: '/layouts/sizes',
    label: 'Sizes',
  },
  {
    to: '/layouts/containers',
    label: 'Containers',
  },
];

export default layoutsMenuItems;
