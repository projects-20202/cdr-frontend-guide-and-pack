import * as React from 'react';
import { Page } from '../../../pack/index';

const LayoutsMainPage: () => JSX.Element = () => {
  return (
    <Page>
      <div className="section-container">
        <h1>Hello LayoutsMainPage</h1>
      </div>
    </Page>
  );
};

export default LayoutsMainPage;
