const mvcMenuItems = [
  {
    to: '/mvc/basics',
    label: 'Basics',
  },
  {
    to: '/mvc/storage',
    label: 'Storage',
  },
  {
    to: '/mvc/navigation',
    label: 'Navigation',
  },
  {
    to: '/mvc/array',
    label: 'Array-State',
  },
  {
    to: '/mvc/text-loader',
    label: 'Text-Loader',
  },
];

export default mvcMenuItems;
