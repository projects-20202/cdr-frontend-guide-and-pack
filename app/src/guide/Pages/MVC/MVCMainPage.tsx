import * as React from 'react';
import { Page } from '../../../pack/index';

const MVCMainPage: () => JSX.Element = () => {
  return (
    <Page>
      <div className="section-container">
        <h1>Hello MVCMainPage</h1>
      </div>
    </Page>
  );
};

export default MVCMainPage;
