import * as React from 'react';
import { Page } from '../../../pack/index';
import ExampleMVCWithBasicStorage from '../../Components/Organisms/MVC/ExampleMVCWithBasicStorage';

const MVCStoragePage: () => JSX.Element = () => {
  return (
    <Page>
      <div className="section-container">
        <h1>Hello MVCStoragePage</h1>
        <ExampleMVCWithBasicStorage />
      </div>
    </Page>
  );
};

export default MVCStoragePage;
