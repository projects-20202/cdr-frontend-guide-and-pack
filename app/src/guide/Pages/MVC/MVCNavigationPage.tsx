import * as React from 'react';
import { Page } from '../../../pack/index';
import ExampleRouteNavigation from '../../Components/Organisms/MVC/ExampleRouteNavigation';

const MVCNavigationPage: () => JSX.Element = () => {
  return (
    <Page>
      <div className="section-container">
        <h1>Hello MVCNavigationPage</h1>
        <ExampleRouteNavigation />
      </div>
    </Page>
  );
};

export default MVCNavigationPage;
