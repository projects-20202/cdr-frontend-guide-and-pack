import * as React from 'react';
import { Page } from '../../../pack/index';
import ArrayStateExample from '../../Components/Organisms/MVC/ArrayStateExample';

const MVCArrayStatePage: () => JSX.Element = () => {
  return (
    <Page>
      <div className="section-container">
        <h1>Hello MVCArrayStatePage</h1>
        <ArrayStateExample />
      </div>
    </Page>
  );
};

export default MVCArrayStatePage;
