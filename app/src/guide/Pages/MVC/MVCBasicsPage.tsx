import * as React from 'react';
import { Page } from '../../../pack/index';
import ExampleMVCWithBasicStates from '../../Components/Organisms/MVC/ExampleMVCWithBasicStates';

const MVCBasicsPage: () => JSX.Element = () => {
  return (
    <Page>
      <div className="section-container">
        <h1>Hello MVCBasicsPage</h1>
        <ExampleMVCWithBasicStates />
      </div>
    </Page>
  );
};

export default MVCBasicsPage;
