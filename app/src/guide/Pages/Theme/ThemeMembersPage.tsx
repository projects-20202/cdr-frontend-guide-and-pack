import * as React from 'react';
import { useState } from 'react';
import {
  BiColor,
  CheckBoxButton,
  LandscapePicsBases,
  lorem,
  Menu,
  ModalFormGroup,
  ModalWindowFluid,
  Organism,
  Page,
  PicsBases,
  SizedContent,
  useMiniScreen,
  useSmallScreen,
  useTinyScreen,
} from '../../../pack/index';
import { MembersControllerContract, useMembersController } from '../../Controllers/Themes/MembersController';
import { MemberModel } from '../../Models/MemberModel';
import ModalForm from '../../../pack/Layouts/Components/ModalForm';

const genMemberMosaicPicture = () => {
  const countPictures = Math.floor(Math.random() * 50);
  const mosaicPictures = [];
  const colors = [
    'color-omega-fade-light',
    'color-omega-pastel-light',
    'color-omega-bright-light',
    'color-omega-fade-dusky',
    'color-omega-pastel-dusky',
    'color-omega-bright-dusky',
  ];
  for (let i = 0; i < countPictures; i++) {
    mosaicPictures.push(
      <SizedContent key={i} className={colors[Math.floor(Math.random() * 6)] + ' center'} {...PicsBases.TINY}>
        {lorem.generateNames(1)}
      </SizedContent>,
    );
  }
  return mosaicPictures;
};

const MemberAbstract = (props: { member: MemberModel }) => {
  const mosaic = genMemberMosaicPicture();

  return (
    <div className="section-container member-abstract">
      <SizedContent {...PicsBases.SMALL} className="color-delta-bright-light center">
        {lorem.generateNames(1)}
      </SizedContent>
      {mosaic.length > 0 ? (
        <div className="member-mosaic">{genMemberMosaicPicture()}</div>
      ) : (
        <div className="member-mosaic-empty center maximize">No Picture Uploaded</div>
      )}
      <div>
        <SizedContent
          className="color-delta-fade-dusky article-container overflow"
          w={LandscapePicsBases.SMALL.w}
          h={LandscapePicsBases.SMALL.h / 2}
        >
          {props.member.name}
          <br />
          {props.member.description}
        </SizedContent>
        <BiColor
          firstColorClass="color-delta-fade-light"
          secondColorClass="color-delta-fade-dusky"
          w={LandscapePicsBases.SMALL.w}
          h={LandscapePicsBases.SMALL.h / 2}
          firstContent={
            <pre>
              {props.member.name}
              {'\n'}
              {props.member.city}
              {'\n'}
              {props.member.status}
            </pre>
          }
          secondContent={
            <pre>
              {props.member.distance}km{'\n'}
              {props.member.clothSize} / {props.member.shoesSize}
            </pre>
          }
        />
      </div>
    </div>
  );
};

const MemberAbstractSmall = (props: { member: MemberModel }) => {
  const small = useSmallScreen();
  const mosaic = genMemberMosaicPicture();

  return (
    <div className="section-container member-abstract">
      <div className="member-abstract-description">
        <SizedContent className="color-delta-fade-dusky article-container overflow" h={LandscapePicsBases.SMALL.h / 2}>
          {props.member.name}
          {small && (
            <>
              <br />
              {props.member.description}
            </>
          )}
        </SizedContent>
        <BiColor
          firstColorClass="color-delta-fade-light"
          secondColorClass="color-delta-fade-dusky"
          w={LandscapePicsBases.SMALL.w}
          h={LandscapePicsBases.SMALL.h / 2}
          firstContent={
            <pre>
              {props.member.name}
              {'\n'}
              {props.member.city}
              {'\n'}
              {props.member.status}
            </pre>
          }
          secondContent={
            <pre>
              {props.member.distance}km{'\n'}
              {props.member.clothSize} / {props.member.shoesSize}
            </pre>
          }
        />
      </div>
      <div className="member-abstract-pictures">
        {mosaic.length > 0 ? (
          <div className="member-mosaic">{genMemberMosaicPicture()}</div>
        ) : (
          <div className="member-mosaic-empty center maximize">No Picture Uploaded</div>
        )}
        <SizedContent {...PicsBases.SMALL} className="color-delta-bright-light center">
          {lorem.generateNames(1)}
        </SizedContent>
      </div>
    </div>
  );
};

const MembersFilteringModal: (props: { memberController: MembersControllerContract }) => JSX.Element = (props) => {
  const memberController = props.memberController;

  const items = [
    {
      content: 'proximity',
      control: (
        <CheckBoxButton
          active={memberController.filter.proximity}
          toggle={() => memberController.filter.setProximity(!memberController.filter.proximity)}
        />
      ),
    },
    {
      content: 'size',
      control: (
        <CheckBoxButton
          active={memberController.filter.size}
          toggle={() => memberController.filter.setSize(!memberController.filter.size)}
        />
      ),
    },
    {
      content: 'count',
      control: (
        <CheckBoxButton
          active={memberController.filter.countClothes}
          toggle={() => memberController.filter.setCountClothes(!memberController.filter.countClothes)}
        />
      ),
    },
  ];

  return (
    <Organism>
      <ModalForm>
        <ModalFormGroup title={'Filters'} items={items} />
      </ModalForm>
    </Organism>
  );
};

const ThemeMembersPage: () => JSX.Element = () => {
  const [filteringFirst, setFilteringFirst] = useState<boolean>();
  const membersController = useMembersController();

  const mini = useMiniScreen();
  const tiny = useTinyScreen();
  const small = useSmallScreen();
  const smallScreen = mini || tiny || small;

  const closeModal = () => setFilteringFirst(false);

  const tMenuItems = [
    {
      action: () => setFilteringFirst(!filteringFirst),
      label: 'FILTER',
    },
  ];

  return (
    <Page>
      <nav className="nav-bar">
        <Menu items={tMenuItems} />
      </nav>
      <div className="section-container">
        {filteringFirst && (
          <ModalWindowFluid onClose={closeModal}>
            <MembersFilteringModal memberController={membersController} />
          </ModalWindowFluid>
        )}
      </div>
      {membersController.members.map((member: MemberModel, index) => {
        return (
          <>
            {smallScreen ? (
              <MemberAbstractSmall key={index} member={member} />
            ) : (
              <MemberAbstract key={index} member={member} />
            )}
            {index !== membersController.members.length - 1 && <hr />}
          </>
        );
      })}
    </Page>
  );
};

export default ThemeMembersPage;
