import * as React from 'react';
import { displaySize, lorem, Page, SizedContent, useMemberPageLayoutSizes, useScreenWidth } from '../../../pack/index';

const ThemeMemberPage: () => JSX.Element = () => {
  const sizes = useMemberPageLayoutSizes();
  const screen = useScreenWidth();
  const displayGaps = screen !== 'mini' && screen !== 'tiny';

  const mainPictureSize = sizes.mainPictureSize;
  const secondaryPictureSize = sizes.secondaryPictureSize;
  const secondaryPictureLegendSize = sizes.secondaryPictureLegendSize;
  const bandSize = sizes.bandSize;
  const gapSize = sizes.gapSize;

  const genRandomMemberPicture = (i: number) => {
    return (
      <SizedContent h={secondaryPictureSize.h + secondaryPictureLegendSize.h} key={i}>
        <SizedContent {...secondaryPictureSize} className="color-beta-brighter-light center">
          {lorem.generateNames(1)}
        </SizedContent>
        <SizedContent {...secondaryPictureLegendSize} className="color-beta-fader-light little-container">
          <div>{lorem.generateWords(10)}</div>
        </SizedContent>
      </SizedContent>
    );
  };

  const genRandomMemberPictures = () => {
    const t = Math.ceil(Math.random() * 100);
    const pictures = [];
    for (let i = 0; i < t; i++) {
      pictures.push(genRandomMemberPicture(i));
    }
    return pictures;
  };

  return (
    <Page>
      <div className="member-layer-presentation">
        <SizedContent {...mainPictureSize} className="color-delta-pastel-light center">
          MAIN PIC {displaySize(mainPictureSize)}
        </SizedContent>
        {displayGaps && <SizedContent {...gapSize} />}
        <SizedContent {...bandSize} className="color-alpha-pastel-dusky little-container">
          {lorem.generateWords(3)}
          <br />
          {lorem.generateWords(3)}
          <br />
          {lorem.generateWords(3)}
        </SizedContent>
        {displayGaps && <SizedContent {...gapSize} />}
        <SizedContent {...bandSize} className="color-alpha-pastel-dusky little-container">
          AAA
        </SizedContent>
      </div>
      <div className="member-layer-all-pictures">{genRandomMemberPictures()}</div>
    </Page>
  );
};

export default ThemeMemberPage;
