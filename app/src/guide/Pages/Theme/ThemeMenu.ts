const themeMenuItems = [
  {
    to: '/themes/member',
    label: 'Member',
  },
  {
    to: '/themes/members',
    label: 'Members',
  },
  {
    to: '/themes/cloth',
    label: 'Cloth',
  },
  {
    to: '/themes/clothes',
    label: 'Clothes',
  },
  {
    to: '/themes/calendar',
    label: 'Calendar',
  },
];

export default themeMenuItems;
