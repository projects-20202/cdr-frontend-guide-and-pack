import * as React from 'react';
import {
  LandscapePicsBases,
  lorem,
  Page,
  PicsBases,
  SizedContent,
  useMiniScreen,
  useSmallScreen,
  useTinyScreen,
} from '../../../pack/index';
import { useClothesController } from '../../Controllers/Themes/ClothesController';
import { ClothModel } from '../../Models/ClothModel';

const genClothMosaicPicture = () => {
  const countPictures = Math.floor(Math.random() * 50);
  const mosaicPictures = [];
  const colors = [
    'color-omega-fade-light',
    'color-omega-pastel-light',
    'color-omega-bright-light',
    'color-omega-fade-dusky',
    'color-omega-pastel-dusky',
    'color-omega-bright-dusky',
  ];
  for (let i = 0; i < countPictures; i++) {
    mosaicPictures.push(
      <SizedContent key={i} className={colors[Math.floor(Math.random() * 6)] + ' center'} {...PicsBases.TINY}>
        {lorem.generateNames(1)}
      </SizedContent>,
    );
  }
  return mosaicPictures;
};

const ClothAbstract = (props: { cloth: ClothModel }) => {
  const mosaic = genClothMosaicPicture();

  return (
    <div className="section-container cloth-abstract">
      <SizedContent {...PicsBases.SMALL} className="color-delta-bright-light center">
        {lorem.generateNames(1)}
      </SizedContent>
      {mosaic.length > 0 ? (
        <div className="cloth-mosaic">{mosaic}</div>
      ) : (
        <div className="cloth-mosaic-empty center maximize">No Picture Uploaded</div>
      )}
      <div>
        <SizedContent
          className="color-delta-fade-dusky article-container overflow"
          w={LandscapePicsBases.SMALL.w}
          h={LandscapePicsBases.SMALL.h / 2}
        >
          CLOTH BRAND
        </SizedContent>
        <SizedContent
          className="color-delta-fade-light article-container overflow"
          w={LandscapePicsBases.SMALL.w}
          h={LandscapePicsBases.SMALL.h / 2}
        >
          CLOTH DESCRIPTION
        </SizedContent>
      </div>
    </div>
  );
};

const ClothAbstractSmall = (props: { cloth: ClothModel }) => {
  const mosaic = genClothMosaicPicture();

  return (
    <div className="section-container cloth-abstract">
      <div className="cloth-abstract-description">
        <SizedContent className="color-delta-fade-dusky article-container overflow" h={LandscapePicsBases.SMALL.h / 2}>
          CLOTH BRAND
        </SizedContent>
        <SizedContent
          className="color-delta-fade-light article-container overflow"
          w={LandscapePicsBases.SMALL.w}
          h={LandscapePicsBases.SMALL.h / 2}
        >
          CLOTH DESCRIPTION
        </SizedContent>
      </div>
      <div className="cloth-abstract-pictures">
        {mosaic.length > 0 ? (
          <div className="cloth-mosaic">{mosaic}</div>
        ) : (
          <div className="cloth-mosaic-empty center maximize">No Picture Uploaded</div>
        )}
        <SizedContent {...PicsBases.SMALL} className="color-delta-bright-light center">
          {lorem.generateNames(1)}
        </SizedContent>
      </div>
    </div>
  );
};

const ThemeClothesPage: () => JSX.Element = () => {
  const mini = useMiniScreen();
  const tiny = useTinyScreen();
  const small = useSmallScreen();
  const smallScreen = mini || tiny || small;
  const clothesController = useClothesController();

  return (
    <Page>
      {clothesController.clothes.map((cloth: ClothModel, index) => {
        return (
          <>
            {smallScreen ? (
              <ClothAbstractSmall key={index} cloth={cloth} />
            ) : (
              <ClothAbstract key={index} cloth={cloth} />
            )}
            {index !== clothesController.clothes.length - 1 && <hr />}
          </>
        );
      })}
    </Page>
  );
};

export default ThemeClothesPage;
