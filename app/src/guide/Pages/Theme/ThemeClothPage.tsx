import * as React from 'react';
import { displaySize, Page, SizedContent } from '../../../pack/index';
import useClothPageLayoutSizes from '../../../pack/Layouts/Pages/ClothLayout';

const ThemeClothPage: () => JSX.Element = () => {
  const sizes = useClothPageLayoutSizes();

  const mainPictureSize = sizes.mainPictureSize;
  const brandSize = sizes.brandSize;
  const calendarSize = sizes.calendarSize;
  const descriptionSize = sizes.descriptionSize;
  const secondaryPictureSize = sizes.secondaryPictureSize;
  const recommendationPictureSize = sizes.recommendationPictureSize;
  const recommendationPictureLegendSize = sizes.recommendationPictureLegendSize;

  const genSecondaryPictures = () => {
    const countSecondaryPictures =
      Math.random() < 0.5 ? Math.floor(Math.random() * 14) : Math.floor(Math.random() * 12);
    const pictures = [];
    for (let i = 0; i < countSecondaryPictures; i++) {
      pictures.push(
        <SizedContent {...secondaryPictureSize} className="color-omega-bright-light center">
          Pic-S - {displaySize(secondaryPictureSize)}
        </SizedContent>,
      );
    }
    return pictures;
  };
  const secondaryPictures = genSecondaryPictures();

  const genRecommendationPictures = () => {
    const countRecommendations = Math.floor(Math.random() * 6);
    const pictures = [];
    for (let i = 0; i < countRecommendations; i++) {
      pictures.push(
        <div>
          <SizedContent {...recommendationPictureSize} className="color-omega-pastel-light center">
            Pic-R
          </SizedContent>
          <SizedContent {...recommendationPictureLegendSize} className="color-omega-fade-light center">
            Legend
          </SizedContent>
        </div>,
      );
    }
    return pictures;
  };
  const recommendations = genRecommendationPictures();
  return (
    <Page>
      <div className="section-container">
        <div className="cloth-container">
          <div className="cloth-layer-main-picture">
            <SizedContent {...mainPictureSize} className="color-alpha-pastel-dusky center">
              MAIN - {displaySize(mainPictureSize)}
            </SizedContent>
          </div>
          <div className="cloth-layer-brand">
            <SizedContent {...brandSize} className="color-beta-pastel-light center">
              BRAND - {displaySize(brandSize)}
            </SizedContent>
          </div>
          <div className="cloth-layer-description">
            <SizedContent {...descriptionSize} className="color-gamma-pastel-light center">
              Description - {displaySize(descriptionSize)}
            </SizedContent>
          </div>
          <div className="cloth-layer-calendar">
            <SizedContent {...calendarSize} className="color-delta-pastel-light center">
              Calendar - {displaySize(calendarSize)}
            </SizedContent>
          </div>
          {secondaryPictures.length > 0 ? (
            <div className="cloth-layer-pictures">{secondaryPictures}</div>
          ) : (
            <SizedContent className="cloth-layer-pictures-empty center">
              Be the first one to rent this and upload your pictures
            </SizedContent>
          )}
          <div className="cloth-layer-recommendations">{recommendations}</div>
        </div>
      </div>
    </Page>
  );
};

export default ThemeClothPage;
