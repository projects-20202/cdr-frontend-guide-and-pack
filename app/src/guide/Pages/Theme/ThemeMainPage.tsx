import * as React from 'react';
import { Page } from '../../../pack/index';

const ThemeMainPage: () => JSX.Element = () => {
  return (
    <Page>
      <div className="section-container">
        <h1>Hello ThemeMainPage</h1>
      </div>
    </Page>
  );
};

export default ThemeMainPage;
