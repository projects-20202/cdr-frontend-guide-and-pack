import * as React from 'react';
import { Page, PortraitShape, SizedContent } from '../../../pack/index';

const ThemeCalendarPage: () => JSX.Element = () => {
  const calendarLine = () => (
    <div className="calendar-line">
      <div className="calendar-line-month">Month</div>
      <div className="calendar-line-year">2020</div>
      {Math.random() > 0.5 ? (
        <div className="icon-square calendar-line-control available" />
      ) : (
        <div className="icon-square-full calendar-line-control not-available" />
      )}
    </div>
  );

  return (
    <Page>
      <div className="section-container">
        <h1>Calendar Little</h1>
        <SizedContent {...PortraitShape(156)} className="calendar">
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
        </SizedContent>
      </div>
      <div className="section-container">
        <h1>Calendar Big</h1>

        <SizedContent {...PortraitShape(300)} className="calendar">
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
          {calendarLine()}
        </SizedContent>
      </div>
    </Page>
  );
};

export default ThemeCalendarPage;
