import * as React from 'react';
import { Enum, Float, Integer, Page, useTextLoader } from '../../pack/index';
import ReactMarkdown from 'react-markdown';
// eslint-disable-next-line import/no-webpack-loader-syntax
import readmeUrl from '!file-loader!../Documentation/README.md';
// @TODO - Make examples of raw VS file loading for text files in the guide
// eslint-disable-next-line import/no-webpack-loader-syntax
import readmeRow from '!raw-loader!../Documentation/testRow.md';
import '../Documentation/mdToHtml.scss';

type ExampleType = 'v1' | 'v2' | 'v3';
const exampleValues: ExampleType[] = ['v1', 'v2', 'v3'];

const HomePage: () => JSX.Element = () => {
  const mdContent = useTextLoader(readmeUrl as Request | string);

  return (
    <Page>
      <div className="section-container">
        <h1>Developer Guide</h1>
        <article className="md-to-html">
          <ReactMarkdown source={mdContent} />
        </article>
        <h1>Developer Guide again</h1>
        <article className="md-to-html">
          <ReactMarkdown source={readmeRow} />
        </article>
        <h1>Randomize</h1>
        Integer(de 42 à 84 ; multiple de 6) : {new Integer(42, 84, 6).randomize()} <br />
        Float : {new Float(42, 84).randomize()} <br />
        Enum : {new Enum<ExampleType>(exampleValues).randomize()} <br />
      </div>
    </Page>
  );
};

export default HomePage;
