import { ExampleModel, randExampleProvider } from '../../Models/ExampleModel';

export type ExampleSetState = {
  setExampleNumber: (newExampleNumberValue: number) => void;
  setExampleString: (newExampleStringValue: string) => void;
  setExampleBool: (newExampleBoolValue: boolean) => void;
};
export type ExampleState = ExampleModel & ExampleSetState;

export type ExampleControllerContract = {
  randomize: () => void;
  currentExample: ExampleModel;
  setCurrentExample: (newExample: ExampleModel) => void;
} & ExampleSetState;

export const stateToController: (exampleState: ExampleState) => ExampleControllerContract = (exampleState) => {
  const setCurrentExample: (newExampleValue: ExampleModel) => void = (newExampleValue) => {
    exampleState.setExampleString(newExampleValue.exampleString);
    exampleState.setExampleNumber(newExampleValue.exampleNumber);
    exampleState.setExampleBool(newExampleValue.exampleBool);
  };

  const randomize: () => void = () => {
    const newExampleValue: ExampleModel = randExampleProvider();
    setCurrentExample(newExampleValue);
  };

  const currentExample: ExampleModel = {
    exampleString: exampleState.exampleString,
    exampleNumber: exampleState.exampleNumber,
    exampleBool: exampleState.exampleBool,
  };

  return {
    randomize,
    currentExample,
    setCurrentExample,
    setExampleString: exampleState.setExampleString,
    setExampleNumber: exampleState.setExampleNumber,
    setExampleBool: exampleState.setExampleBool,
  };
};
