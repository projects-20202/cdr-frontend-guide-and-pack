import { ExampleControllerContract, ExampleState, stateToController } from './ExampleControllerCommon';
import { useLocalStorage } from '../../../pack/index';

////////////////////////////////////////////////////////////
// An MVC implementation using Hooks in React+TS
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// The M , semantic data, generally provided by the backend
////////////////////////////////////////////////////////////

// @SEE ExampleModel.ts

////////////////////////////////////////////////////////////
// The C , will provide the data, and a set of methods to alter the data, to the frontend
////////////////////////////////////////////////////////////

// first : data & data setters that represent a state of the UX (eg: an Item can be selected or not, a RootItem also can be expanded or not)

// @SEE ExampleControllerCommon

// second : I create method that will add the control methods to the model data retrieved at the 'M' stage.

const useExampleStorage: () => ExampleState = () => {
  const [exampleNumber, setExampleNumber] = useLocalStorage<number>('example-number', 0);
  const [exampleString, setExampleString] = useLocalStorage<string>('example-string', '');
  const [exampleBool, setExampleBool] = useLocalStorage<boolean>('example-bool', false);

  return {
    exampleNumber,
    exampleString,
    exampleBool,
    setExampleNumber,
    setExampleString,
    setExampleBool,
  };
};

// FINALLY, I create the global controller that retrieve the data, transform them to controllers and return it to the View

// @SEE ExampleControllerCommon

export const useExampleStorageController: () => ExampleControllerContract = () => {
  const exampleState: ExampleState = useExampleStorage();

  return stateToController(exampleState);
};

////////////////////////////////////////////////////////////
// The V , it is the React Components
////////////////////////////////////////////////////////////
