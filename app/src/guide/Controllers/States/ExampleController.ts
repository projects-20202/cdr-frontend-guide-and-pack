import { useState } from 'react';
import { ExampleControllerContract, ExampleState, stateToController } from './ExampleControllerCommon';

////////////////////////////////////////////////////////////
// An MVC implementation using Hooks in React+TS
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// The M , semantic data, generally provided by the backend
////////////////////////////////////////////////////////////

// @SEE ExampleModel.ts

////////////////////////////////////////////////////////////
// The C , will provide the data, and a set of methods to alter the data, to the frontend
////////////////////////////////////////////////////////////

// first : data & data setters that represent a state of the UX (eg: an Item can be selected or not, a RootItem also can be expanded or not)

// @SEE ExampleControllerCommon

// second : I create method that will add the control methods to the model data retrieved at the 'M' stage.

const useExampleState: () => ExampleState = () => {
  const [exampleNumber, setExampleNumber] = useState<number>(0);
  const [exampleString, setExampleString] = useState<string>('');
  const [exampleBool, setExampleBool] = useState<boolean>(false);

  return {
    exampleNumber,
    exampleString,
    exampleBool,
    setExampleNumber,
    setExampleString,
    setExampleBool,
  };
};

// FINALLY, I create the global controller that retrieve the data, transform them to controllers and return it to the View

// @SEE ExampleControllerCommon

export const useExampleController: () => ExampleControllerContract = () => {
  const exampleState: ExampleState = useExampleState();

  return stateToController(exampleState);
};

////////////////////////////////////////////////////////////
// The V , it is the React Components
////////////////////////////////////////////////////////////
