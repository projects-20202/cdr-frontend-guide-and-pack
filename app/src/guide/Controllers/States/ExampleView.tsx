import * as React from 'react';
import { ExampleControllerContract } from './ExampleControllerCommon';
import { InputNumber, InputText, MainForm, Organism, Toggle } from '../../../pack/index';

const ExampleView: (exampleController: ExampleControllerContract) => JSX.Element = (exampleController) => {
  const toggleExample = () => {
    exampleController.setExampleBool(!exampleController.currentExample.exampleBool);
  };

  return (
    <Organism>
      <div className="center">
        <MainForm>
          <h1>This is an example Form</h1>
          <InputText
            label="Example Text"
            value={exampleController.currentExample.exampleString}
            mapChange={exampleController.setExampleString}
            id="example-text"
          />
          <InputNumber
            label="Example Number"
            value={exampleController.currentExample.exampleNumber}
            mapChange={exampleController.setExampleNumber}
            id="example-number"
          />
          <h2>More advanced components</h2>
          <label>You can toggle me</label>
          <Toggle active={exampleController.currentExample.exampleBool} toggle={toggleExample} />
        </MainForm>
      </div>
      <h1>exampleController.currentExample</h1>
      <pre>{JSON.stringify(exampleController.currentExample, null, 2)}</pre>
    </Organism>
  );
};

export default ExampleView;
