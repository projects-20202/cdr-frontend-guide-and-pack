import { MemberFilteringModel, MemberModel, membersProvider } from '../../Models/MemberModel';
import { useLocalStorage } from '../../../pack/index';

type MembersFilteringState = MemberFilteringModel & {
  setProximity: (newProximity: boolean) => void;
  setSize: (newSize: boolean) => void;
  setCountClothes: (newCount: boolean) => void;
};

export type MembersControllerContract = {
  filter: MembersFilteringState;
  members: MemberModel[];
};

export const useMembersController: () => MembersControllerContract = () => {
  const [proximity, setProximity] = useLocalStorage<boolean>('members-filter-by-proximity', true);
  const [size, setSize] = useLocalStorage<boolean>('members-filter-by-size', true);
  const [countClothes, setCountClothes] = useLocalStorage<boolean>('members-filter-by-count-clothes', true);

  const filter: MembersFilteringState = { proximity, size, countClothes, setProximity, setSize, setCountClothes };
  return { filter, members: membersProvider(filter) };
};
