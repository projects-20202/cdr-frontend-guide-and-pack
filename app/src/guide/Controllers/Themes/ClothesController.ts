import { clothesProvider, ClothModel } from '../../Models/ClothModel';

export type ClothesControllerContract = {
  clothes: ClothModel[];
};

export const useClothesController: () => ClothesControllerContract = () => {
  return { clothes: clothesProvider() };
};
