import {
  CriteriaAFilter,
  CriteriaBFilter,
  CriteriaCFilter,
  CriteriaDFilter,
  CriteriaEFilter,
  FilteringPattern,
  FilteringResult,
  resultProvider,
} from '../../Models/FilteringModels';
import { useState } from 'react';

export type FilteringPatternSetState = {
  setA: (newA: CriteriaAFilter) => void;
  setB: (newA: CriteriaBFilter) => void;
  setC: (newA: CriteriaCFilter) => void;
  setD: (newA: CriteriaDFilter) => void;
  setE: (newA: CriteriaEFilter) => void;
};

export type FilteringPatternState = FilteringPattern & FilteringPatternSetState;

export type FilteringControllerContract = {
  currentFilter: FilteringPattern;
  setFilter: FilteringPatternState;
  getResults: () => FilteringResult[];
};

export const useFilteringController: () => FilteringControllerContract = () => {
  const [a, setA]: [CriteriaAFilter, (newA: CriteriaAFilter) => void] = useState<CriteriaAFilter>(null);
  const [b, setB]: [CriteriaBFilter, (newB: CriteriaBFilter) => void] = useState<CriteriaBFilter>(false);
  const [c, setC]: [CriteriaCFilter, (newC: CriteriaCFilter) => void] = useState<CriteriaCFilter>('');
  const [d, setD]: [CriteriaDFilter, (newD: CriteriaDFilter) => void] = useState<CriteriaDFilter>(null);
  const [e, setE]: [CriteriaEFilter, (newE: CriteriaEFilter) => void] = useState<CriteriaEFilter>({
    v1: true,
    v2: true,
    v3: true,
    v4: true,
    v5: true,
    v6: true,
  });

  const currentFilter: FilteringPattern = { a, b, c, d, e };
  const setFilter = { ...currentFilter, setA, setB, setC, setD, setE };

  const getResults: () => FilteringResult[] = () => {
    const results: FilteringResult[] = [];
    const countResults = Math.floor(Math.random() * 30);
    for (let i = 0; i < countResults; i++) {
      results.push(resultProvider(currentFilter));
    }
    return results;
  };
  return { currentFilter, setFilter, getResults };
};
