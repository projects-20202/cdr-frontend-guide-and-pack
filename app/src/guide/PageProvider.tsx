import * as React from 'react';
import { useRouteMatch } from 'react-router-dom';
import { Menu, MenuItemProps, Organism } from '../pack/index';
import themeMenuItems from './Pages/Theme/ThemeMenu';
import homeMenuItems from './Pages/HomeMenu';
import HomePage from './Pages/HomePage';
import ThemeMainPage from './Pages/Theme/ThemeMainPage';
import ThemeMemberPage from './Pages/Theme/ThemeMemberPage';
import ThemeClothPage from './Pages/Theme/ThemeClothPage';
import colorMenuItems from './Pages/Color/ColorsMenu';
import ColorGuidePage from './Pages/Color/ColorsGuidePage';
import ColorHuesPage from './Pages/Color/ColorsHuesPage';
import ColorGlobalPage from './Pages/Color/ColorsGlobalPage';
import ColorMainPage from './Pages/Color/ColorsMainPage';
import layoutsMenuItems from './Pages/Layouts/LayoutsMenu';
import LayoutsGapsPage from './Pages/Layouts/LayoutsGapsPage';
import LayoutsSizesPage from './Pages/Layouts/LayoutsSizesPage';
import LayoutsContainersPage from './Pages/Layouts/LayoutsContainersPage';
import LayoutsMainPage from './Pages/Layouts/LayoutsMainPage';
import mvcMenuItems from './Pages/MVC/MVCMenu';
import MVCBasicsPage from './Pages/MVC/MVCBasicsPage';
import MVCMainPage from './Pages/MVC/MVCMainPage';
import MVCStoragePage from './Pages/MVC/MVCStoragePage';
import MVCNavigationPage from './Pages/MVC/MVCNavigationPage';
import MVCArrayStatePage from './Pages/MVC/MVCArrayStatePage';
import MVCTextLoaderPage from './Pages/MVC/MVCTextLoaderPage';
import IconMainPage from './Pages/Icon/IconMainPage';
import FilteringPageContent from './Pages/Filtering/FilteringPageContent';
import useFilteringMenu from './Pages/Filtering/FilteringMenu';
import ThemeMembersPage from './Pages/Theme/ThemeMembersPage';
import ThemeClothesPage from './Pages/Theme/ThemeClothesPage';
import ThemeCalendarPage from './Pages/Theme/ThemeCalendarPage';

const PageHeader: () => JSX.Element = () => {
  const themes = useRouteMatch('/themes');
  const colors = useRouteMatch('/colors');
  const icons = useRouteMatch('/icons');
  const layouts = useRouteMatch('/layouts');
  const filtering = useRouteMatch('/filtering');
  const filteringItems = useFilteringMenu();
  const mvc = useRouteMatch('/mvc');

  const genItems: () => MenuItemProps[] = () => {
    if (themes) return themeMenuItems;
    if (colors) return colorMenuItems;
    if (layouts) return layoutsMenuItems;
    if (filtering) return filteringItems;
    if (mvc) return mvcMenuItems;
    if (icons) return homeMenuItems;
    return homeMenuItems;
  };
  return (
    <Organism>
      <Menu items={genItems()} />
    </Organism>
  );
};

const ThemePageContent: () => JSX.Element = () => {
  const memberTheme = useRouteMatch('/themes/member');
  const membersTheme = useRouteMatch('/themes/members');
  const clothTheme = useRouteMatch('/themes/cloth');
  const clothesTheme = useRouteMatch('/themes/clothes');
  const calendarTheme = useRouteMatch('/themes/calendar');

  if (memberTheme) return <ThemeMemberPage />;
  if (membersTheme) return <ThemeMembersPage />;
  if (clothTheme) return <ThemeClothPage />;
  if (clothesTheme) return <ThemeClothesPage />;
  if (calendarTheme) return <ThemeCalendarPage />;

  return <ThemeMainPage />;
};

const ColorPageContent: () => JSX.Element = () => {
  const guide = useRouteMatch('/colors/guide');
  const hues = useRouteMatch('/colors/hues');
  const global = useRouteMatch('/colors/global');

  if (guide) return <ColorGuidePage />;
  if (hues) return <ColorHuesPage />;
  if (global) return <ColorGlobalPage />;
  return <ColorMainPage />;
};

const LayoutPageContent: () => JSX.Element = () => {
  const gaps = useRouteMatch('/layouts/gaps');
  const sizes = useRouteMatch('/layouts/sizes');
  const containers = useRouteMatch('/layouts/containers');

  if (gaps) return <LayoutsGapsPage />;
  if (sizes) return <LayoutsSizesPage />;
  if (containers) return <LayoutsContainersPage />;
  return <LayoutsMainPage />;
};

const MVCPageContent: () => JSX.Element = () => {
  const basics = useRouteMatch('/mvc/basics');
  const storage = useRouteMatch('/mvc/storage');
  const navigation = useRouteMatch('/mvc/navigation');
  const array = useRouteMatch('/mvc/array');
  const loader = useRouteMatch('/mvc/text-loader');

  if (basics) return <MVCBasicsPage />;
  if (storage) return <MVCStoragePage />;
  if (navigation) return <MVCNavigationPage />;
  if (array) return <MVCArrayStatePage />;
  if (loader) return <MVCTextLoaderPage />;
  return <MVCMainPage />;
};

const IconPageContent: () => JSX.Element = () => {
  return <IconMainPage />;
};

const PageContent: () => JSX.Element = () => {
  const themes = useRouteMatch('/themes');
  const colors = useRouteMatch('/colors');
  const icons = useRouteMatch('/icons');
  const layouts = useRouteMatch('/layouts');
  const mvc = useRouteMatch('/mvc');
  const filtering = useRouteMatch('/filtering');

  if (themes) return <ThemePageContent />;
  if (colors) return <ColorPageContent />;
  if (layouts) return <LayoutPageContent />;
  if (filtering) return <FilteringPageContent />;
  if (mvc) return <MVCPageContent />;
  if (icons) return <IconPageContent />;
  return <HomePage />;
};

export { PageHeader, PageContent };
