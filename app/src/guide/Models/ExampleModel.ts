// first I define the data structure

import { lorem } from '../../pack/index';

export type ExampleModel = {
  exampleNumber: number;
  exampleString: string;
  exampleBool: boolean;
};

// second I create a provider to retrieve those data

const randExampleProvider: () => ExampleModel = () => {
  return {
    exampleNumber: Math.ceil(Math.random() * 100),
    exampleString: lorem.generateWords(3),
    exampleBool: Math.random() > 0.5,
  };
};

export { randExampleProvider };
