import { Integer, lorem } from '../../pack/index';

////////////////////////////////////////////////////////////
// The M , semantic data, generally provided by the backend
////////////////////////////////////////////////////////////

// first I define the data structure

export type ClothSizing = {
  cloth: number[];
  shoes: number[];
};

const Categories = [
  'Pants',
  'ShortPants',
  'Shorts',
  'TShirt',
  'Shirt',
  'Sweater',
  'Jacket',
  'Cardigan',
  'Dress',
  'LongDress',
  'ShortDress',
  'Skirt',
  'LongSkirt',
  'ShortSkirt',
  'Hat',
  'HairDressing',
  'Scarf',
  'Necklace',
  'Bracelet',
  'Ring',
  'Earring',
  'Jewelry',
  'Sandals',
  'Shoes',
  'Boots',
];

const Colors = ['Red', 'Orange', 'Brown', 'Yellow', 'Green', 'Blue', 'Purple', 'Pink', 'Black', 'White', 'Gray'];

const Levels = ['Sale', 'Standard', 'TopOfTheLine', 'Luxury'];

const Materials = ['Synthetic', 'Wool', 'Cotton', 'Elastane', 'Velvet', 'Cashmere'];

// noinspection SpellCheckingInspection
const Brands = ['NafNaf', 'IKKS', 'Bash', 'Kiabi', 'Camaieu'];

const Styles = ['Casual', 'Gothic', 'Chic', 'Sporty'];

export type ClothModel = {
  id: number;
  name: string;
  description: string | undefined;
  size: number;
  category: string;
  color: string;
  level: string;
  material: string;
  brand: string;
  style: string;
};

// second I create a provider to CRUD those data

const rand: (values: string[]) => string = (values) => {
  const num = Math.floor(Math.random() * values.length);
  return values[num];
};

export const clothProvider: (clothId: number) => ClothModel = (clothId) => {
  const id = Math.ceil(Math.random() * 200000);
  const size = 2 * Math.ceil(Math.random() * 7 + 16);
  const name = lorem.generateNames(Math.ceil(Math.random() * 3));
  const description = Math.random() > 0.5 ? lorem.generateSentences(1) : '';
  const category: string = rand(Categories);
  const color: string = rand(Colors);
  const level: string = rand(Levels);
  const material: string = rand(Materials);
  const brand: string = rand(Brands);
  const style: string = rand(Styles);

  return { id, size, name, description, category, color, level, material, brand, style };
};

export const clothesProvider: () => ClothModel[] = () => {
  const t = new Integer(10, 30).randomize();
  const clothes: ClothModel[] = [];
  for (let i = 0; i < t; i++) {
    clothes.push(clothProvider(i));
  }
  return clothes;
};
