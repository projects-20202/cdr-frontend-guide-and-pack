import { ClothSizing } from './ClothModel';
import { lorem } from '../../pack/index';

////////////////////////////////////////////////////////////
// The M , semantic data, generally provided by the backend
////////////////////////////////////////////////////////////

// first I define the data structure

type EnumStatus = 'Gold' | 'Silver' | 'Bronze' | 'Anonymous';

const status = () => {
  const n = Math.random() * 5;
  if (n > 4) {
    return 'Gold';
  }
  if (n > 3) {
    return 'Silver';
  }
  if (n > 2) {
    return 'Bronze';
  }
  return 'Anonymous';
};

type MemberAbstract = {
  name: string;
  city: string;
  status: EnumStatus;
  description?: string;
};

type MemberMatching = {
  distance: number;
  clothSize: number;
  shoesSize: number;
};

type Measurements = {
  chest?: number;
  waist?: number;
  hips?: number;
  thigh?: number;
  wrist?: number;
  fingers?: number[];
};

type GeneralMeasure = {
  height?: number;
  weight?: number;
};

type Size = Measurements & ClothSizing & GeneralMeasure;

export type MemberModel = MemberAbstract & MemberMatching;

export type MemberFilteringModel = {
  proximity: boolean;
  size: boolean;
  countClothes: boolean;
};
export const memberFilteringValues = ['proximity', 'size', 'countClothes'];
export type MemberFilteringValue = 'proximity' | 'size' | 'countClothes';
// second I create a provider to CRUD those data

export const memberAbstractProvider: (filter: MemberFilteringModel) => MemberAbstract = (filter) => {
  return {
    name: lorem.generateNames(2),
    city: lorem.generateNames(1),
    status: status(),
    description: Math.random() > 0.5 ? lorem.generateSentences(1) : undefined,
  };
};

export const memberMatchingProvider: (filter: MemberFilteringModel) => MemberMatching = (filter) => {
  return {
    distance: Math.ceil(filter.proximity ? Math.random() * 30 : Math.random() * 200),
    clothSize: Math.ceil(filter.size ? Math.random() : Math.random() * 3),
    shoesSize: Math.ceil(filter.size ? Math.random() : Math.random() * 3),
  };
};

export const memberSizeProvider: () => Size = () => {
  const cloth = 2 * Math.ceil(Math.random() * 7 + 16);
  const shoes = Math.ceil(Math.random() * 6 + 36);
  return {
    chest: Math.ceil(Math.random() * 30 + 80),
    waist: Math.ceil(Math.random() * 40 + 60),
    hips: Math.ceil(Math.random() * 5 + 20),
    thigh: Math.ceil(Math.random() * 15 + 30),
    wrist: Math.ceil(Math.random() * 15 + 30),
    fingers: [
      Math.ceil(Math.random() * 3 + 2),
      Math.ceil(Math.random() * 3 + 2),
      Math.ceil(Math.random() * 3 + 2),
      Math.ceil(Math.random() * 3 + 2),
      Math.ceil(Math.random() * 3 + 2),
    ],
    height: Math.ceil(Math.random() * 50 + 150),
    weight: Math.ceil(Math.random() * 55 + 45),
    cloth: [cloth, cloth + 2],
    shoes: [shoes, shoes + 1],
  };
};

export const memberProvider: (filter: MemberFilteringModel) => MemberModel = (filter) => {
  const abstract = memberAbstractProvider(filter);
  const matching = memberMatchingProvider(filter);
  return { ...abstract, ...matching };
};

export const membersProvider: (filter: MemberFilteringModel) => MemberModel[] = (filter) => {
  const countMembers = Math.floor(Math.random() * 30) + 5;
  const members = [];
  for (let i = 0; i < countMembers; i++) {
    members.push(memberProvider(filter));
  }
  return members;
};
