import { lorem } from '../../pack/index';

////////////////////////////////////////////////////////////
// The M , semantic data, generally provided by the backend
////////////////////////////////////////////////////////////

// first I define the data structure

export type CriteriaAValue = 'Val.A' | 'Val.B' | 'Val.C';
export type CriteriaAFilter = CriteriaAValue | null;
export const aValues: CriteriaAValue[] = ['Val.A', 'Val.B', 'Val.C'];

export type CriteriaBFilter = boolean;
export type CriteriaBValue = boolean;

export type CriteriaCFilter = string;
export type CriteriaCValue = string;

export type CriteriaDFilter = {
  min: number;
  max: number;
} | null;
export type CriteriaDValue = number;

export type CriteriaEFilter = {
  v1: boolean;
  v2: boolean;
  v3: boolean;
  v4: boolean;
  v5: boolean;
  v6: boolean;
};
export type CriteriaEValue = 'v1' | 'v2' | 'v3' | 'v4' | 'v5' | 'v6';
export const eValues: CriteriaEValue[] = ['v1', 'v2', 'v3', 'v4', 'v5', 'v6'];

export type FilteringPattern = {
  a: CriteriaAFilter;
  b: CriteriaBFilter;
  c: CriteriaCFilter;
  d: CriteriaDFilter;
  e: CriteriaEFilter;
};

export type FilteringResult = {
  id: number;
  name: string;
  aValue: CriteriaAValue;
  bValue: CriteriaBValue;
  cValue: CriteriaCValue;
  dValue: CriteriaDValue;
  eValue: CriteriaEValue;
};

const randomE: (eFilter: CriteriaEFilter) => CriteriaEValue = (eFilter) => {
  let t = Math.floor(Math.random() * 6);
  let value: CriteriaEValue = eValues[t];
  if (!(eFilter.v1 || eFilter.v2 || eFilter.v3 || eFilter.v4 || eFilter.v5 || eFilter.v6)) {
    return value;
  }

  do {
    if (eFilter[value]) {
      return value;
    }
    t = (t + 1) % 6;
    value = eValues[t];
  } while (true);
};

export const resultProvider: (filter: FilteringPattern) => FilteringResult = (filter) => {
  const id = Math.floor(Math.random() * 10000);
  return {
    id: Math.floor(Math.random() * 10000),
    name: lorem.generateNames(1),
    aValue: filter.a || aValues[id % 3],
    bValue: filter.b || Math.random() > 0.5,
    cValue: filter.c || lorem.generateWords(2),
    dValue: filter.d ? Math.random() * (filter.d.max - filter.d.min) + filter.d.min : Math.random() * 500,
    eValue: randomE(filter.e),
  };
};
