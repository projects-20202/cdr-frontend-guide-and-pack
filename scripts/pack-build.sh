#!/usr/bin/env bash

rm -rf ../pack/lib
rm -rf ../pack/styles
rm -rf ../pack/src
cp -r ../app/src/pack ../pack/src
sh pack-npm.sh run build